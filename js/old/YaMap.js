ymaps.ready(YaMapsInit)

function YaMapsInit() {
  var myMap = new ymaps.Map(
    "map", {
      center : [55.7116879, 37.5813585],
      zoom : 15,
      controls : ['zoomControl', 'typeSelector', 'fullscreenControl']
    }
  );
  myMap.behaviors.disable(['scrollZoom']);
  var myPlacemark = new ymaps.Placemark(
    myMap.getCenter(), {
      hintContent : '119334, Россия, Москва, парк им. Горького, Ленинский пр-т, 30А<br/>+7 499 398-17-33'
    }, {
      iconLayout : 'default#image',
      iconImageHref : '/img/pin.png',
      iconImageSize : [32, 41],
      iconImageOffset : [0, -41],
      balloonContentBody : '',
      balloonContentFooter : ''
    }
  );
  myMap.geoObjects.add(myPlacemark);
}
