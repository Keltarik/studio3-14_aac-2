(function () {
  'use strict';

  function isIE() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf('rv:');
      return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
      // IE 12 => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
  }

  if (!isIE())
    document.body.classList.add('is-not-IE');

  $('.review-video-link').magnificPopup({
    type : 'iframe',
    removalDelay : 300,
    mainClass : 'mfp-fade',
    closeMarkup : '<button title="%title%" class="mfp-close"></button>'
  });

  $(document).on('submit', 'form', function (event) {
    event.stopPropagation();
    event.preventDefault();

    var jThis = $(this)
      , url = jThis.attr('action')
      , data = jThis.serializeArray()
      , error = true
      , l = data.length;

    while (l > 0 && error) {
      l--;
      if ((data[l].name === 'AddReport[user_phone]')
        && data[l].value && data[l].value.length > 8) {
          var count = (data[l].value.match(/\d/g) || []).length;
          //console.log(count)
          if (count > 8 && count < 13) {
             error = false;
          }
	}
    }
    if (!error) {
      $('input[type="submit"]').attr('disabled', true);
      $.post(url, data)
        .done(function () {
          //alert('Спасибо за заявку!\nС вами свяжутся в ближайшее время.');
          window.location.href = '/thankyou';
        }).fail(function () {
          alert('Данные не отправились!! ');
          $('input[type="submit"]').attr('disabled', false);
        });
    } else {
      $.magnificPopup.open({
        type : 'inline',
        removalDelay : 300,
        mainClass : 'mfp-fade',
        closeMarkup : '<button title="%title%" class="mfp-close"></button>',
        items : {
          src : '#modal_form'
        }
      });
    }

  });

  var image_over = $('.image_over');

  if (image_over.length > 0)
    image_over.maphilight({
      stroke : false,
      fillColor : "db0011",
      fillOpacity : .2
    });

  $('.social-share').each(function () {
    var data = $(this).data('social');
    $(this).click(function (event) {
      event.stopPropagation();
      event.preventDefault();
      Share[data](
        window.location.href,
        document.title,
        window.location.origin + "/img/1/back_1.jpg",
        ''
      );
    })
  });

}());

var Share = {
  vkontakte : function (purl, ptitle, pimg, text) {
    url = 'http://vkontakte.ru/share.php?';
    url += 'url=' + encodeURIComponent(purl);
    Share.popup(url);
  },
  odnoklassniki : function (purl, text) {
    url = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
    url += '&st.comments=' + encodeURIComponent(text);
    url += '&st._surl=' + encodeURIComponent(purl);
    Share.popup(url);
  },
  facebook : function (purl, ptitle, pimg, text) {
    url = 'http://www.facebook.com/sharer.php?s=100';
    url += '&p[title]=' + encodeURIComponent(ptitle);
    url += '&p[summary]=' + encodeURIComponent(text);
    url += '&p[url]=' + encodeURIComponent(purl);
    url += '&p[images][0]=' + encodeURIComponent(pimg);
    Share.popup(url);
  },
  twitter : function (purl, ptitle) {
    url = 'http://twitter.com/share?';
    url += 'text=' + encodeURIComponent(ptitle);
    url += '&url=' + encodeURIComponent(purl);
    url += '&counturl=' + encodeURIComponent(purl);
    Share.popup(url);
  },
  mailru : function (purl, ptitle, pimg, text) {
    url = 'http://connect.mail.ru/share?';
    url += 'url=' + encodeURIComponent(purl);
    url += '&title=' + encodeURIComponent(ptitle);
    url += '&description=' + encodeURIComponent(text);
    url += '&imageurl=' + encodeURIComponent(pimg);
    Share.popup(url)
  },

  popup : function (url) {
    window.open(url, '', 'toolbar=0,status=0,width=626,height=436');
  }
};
