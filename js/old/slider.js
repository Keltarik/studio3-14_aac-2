(function () {
  'use strict';

  var jSliders = $('.slider-slide');
  var jNavs = $('.slider-nav-link');
  var jDocument = $(document);
  var currentSlide = 0;
  var interval;

  var Wrapper = $('.wrapper')[0];
  var Images = $('.wrapper-img')[0];
  if (Images && Images.getAttribute('data-imgs'))
    var backImagesSrc = Images.getAttribute('data-imgs').split(',');

  window.stopSlider = function () {
    if (interval)
      clearInterval(interval);
    interval = undefined;
  };
  window.startSlider = function () {
    if (interval)
      return;
    interval = setInterval(next, 7000);
  };

  jDocument.on('mousedown', '.slider-nav-link', function (event) {
    event.stopPropagation();
    event.preventDefault();

    var jThis = $(this)
      , target = jThis.data('target');
    showSlide(target);
  });

  jDocument.on('mouseover', '.slider-nav, .slider-slide-link', function (event) {
    stopSlider();
  });

  jDocument.on('mouseout', '.slider-nav, .slider-slide-link', function (event) {
    startSlider();
  });

  function showSlide(target) {
    if (currentSlide == target)
      return;
    jNavs[currentSlide].classList.remove('active');
    jSliders[currentSlide].classList.remove('in');

    jNavs[target].classList.add('active');
    jSliders[target].classList.add('in');
    Images.style.backgroundImage = "url(" + backImagesSrc[target] + ")";
    Wrapper.style.backgroundImage = "url(" + backImagesSrc[target] + ")";
    currentSlide = target;
  }

  function next() {
    var target = (currentSlide + 1 >= jSliders.length) ? 0 : currentSlide + 1;
    showSlide(target);
  }


  startSlider();
}());