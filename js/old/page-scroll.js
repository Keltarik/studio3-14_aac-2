(function () {
  'use strict';
  var header = $('.header');
  var jDocument = $(document);

  var fixedHeader;

  jDocument.on('scroll', function (event) {
    var jScroll = jDocument.scrollTop();

    if (jScroll >= 1) {
      if (typeof fixedHeader !== "undefined" || !fixedHeader)
        header.addClass('header__static');
      fixedHeader = true;
    } else {
      if (typeof fixedHeader !== "undefined" || fixedHeader)
        header.removeClass('header__static');
      fixedHeader = false;
    }

  });

  var body = $("html, body");

  jDocument.on('click', '.js-scroll-link', function (event) {
    event.stopPropagation();
    event.preventDefault();

    var node = $(this.getAttribute('href'));

    if (node.length <= 0)
      window.location = window.location.origin + "index.html" + this.getAttribute('href');
    body.animate({
      scrollTop : node.offset().top - 100
    }, 300);
  });
}());