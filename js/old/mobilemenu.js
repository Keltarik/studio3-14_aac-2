/**
 * Created by Dmitry Bezverkhiy on 19/01/2017.
 */

(function mobilemenu($) {
    "useStict";

    function onReady() {
        var button = document.getElementsByClassName("mobile-menu")[0];
        var menu = document.getElementsByClassName("navbar-nav")[0];
        var phone = document.getElementsByClassName("navbar-right")[0];

        function Menu(button, menu, phone) {
            if ($(button).css("display") === "none") {
                this.visible = true;
            } else {
                this.visible = false;
            }
            this.button = button;
            this.menu = menu;
            this.phone = phone;
            this.addHandlers();
            this.updateMenuVisibility();
        }

        Menu.prototype.addHandlers = function () {
            this.button.addEventListener("click", this.toggleVisibility.bind(this));
        };

        Menu.prototype.toggleVisibility = function () {
            this.visible = !this.visible;
            this.updateMenuVisibility();
        };

        Menu.prototype.updateMenuVisibility = function () {
            if (this.visible) {
                this.menu.style.display = "block";
                this.phone.style.display = "block";
            } else {
                this.menu.style.display = "none";
                this.phone.style.display = "none";
            }
        };

        var menu = new Menu(button, menu, phone);
    }

    $(document).ready(onReady);

})(jQuery);
