<?php

$params = array_merge(
	require(__DIR__ . '/../../common/config/params.php'),
	require(__DIR__ . '/params.php')
);


$config = [
	'id' => 'frontend',
	'basePath' => dirname(__DIR__),
	'bootstrap' => ['log'],

	'language' => 'ru-RU',
	'sourceLanguage' => 'ru-RU',

	'defaultRoute' => 'site',

	'controllerNamespace' => 'frontend\controllers',

	'components' => [
		'request' => [
			// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
			'cookieValidationKey' => 'Sma1VeQfRTlX_tJ8zApF7p3NTybbyTDP',
		],
		'urlManager' => array(
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'rules' => [
				'<alias:index|consult|about|partners|thankyou|portfolio|callback>' => 'site/<alias>',
				'project/<slug:[-a-zA-Z0-9]+>' => 'site/project',
				'person/<id:[-a-zA-Z0-9]+>' => 'site/person',
                'partner/<id:[-a-zA-Z0-9]+>' => 'site/partner',
			]
		),
		'errorHandler' => [
			'errorAction' => 'site/error',
		]
	],
	'params' => $params,
];
return $config;