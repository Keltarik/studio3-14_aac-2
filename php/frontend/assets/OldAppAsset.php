<?php
/**
 * Created by PhpStorm.
 * User: macos
 * Date: 08.10.17
 * Time: 20:37
 */

namespace frontend\assets;

use yii\web\AssetBundle;

class OldAppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'assets/bootstrap.css',
        'style/old/index.css',
        'style/old/how-work.css',
        'style/old/services.css',
        'style/old/adaptive.css',
        'assets/magnific-popup.css',
    ];

    public $js = [
        'assets/jquery.magnific-popup.min.js',
        'js/old/YaCounter.js',
        'js/old/slider.js',
        'js/old/scroll.js',
        'js/old/service.js',
        'js/old/person.js',
        'js/old/price.js',
        'js/old/index.js',
        'js/old/how-work.js',
        'js/old/mobilemenu.js',
        'assets/bootstrap.min.js',
    ];
}