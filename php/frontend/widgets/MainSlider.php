<?php

namespace frontend\widgets;

use common\models\type\Slider;
use yii\base\Widget;

class MainSlider extends Widget
{
    public function run()
    {
        $model = Slider::find()
            ->orderBy(['position' => SORT_ASC])
            ->all();

        return $this->render('main-slider', [
            'model' => $model
        ]);
    }
}