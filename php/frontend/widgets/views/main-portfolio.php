<?php

/* @var $this \yii\web\View */
/* @var $tagModel \common\models\type\Tag */
/* @var $items array */

$count = 1;
?>


<?php if ($items): ?>
    <div class="wrapper">
        <h2 class="project__title title-line">Наши проекты</h2>
        <?php if($tagModel): ?>
            <ul class="project__nav">
                <li>
                    <a data-rel="all" href="javascript:;" class="active">Все</a>
                </li>
                <?php foreach ($tagModel as $tag): ?>
				<?php
					if( $tag['slug'] == 'Doma_i_taunhausy') 
					{
						continue;
					}
					?>
                    <li>
                        <a data-rel="<?= $tag['slug'] ?>" href="javascript:;"><?= $tag['name'] ?></a>
                    </li>
					
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
        <div id="portfolio">
		<?php 
		//print_r($items);
		$cou = count($items);
		$i =0;
		?>
            <?php foreach ($items as $item): ?>
		        <div class="project__info all <?= $item['tag_slug'] ?> <?= $count > 4 ? 'project__info-small' : '' ?>">
                    <img class="project__info-img" src="<?= Yii::getAlias('@web') . $item['image_url'] ?>" alt="<?= $item['title'] ?>">
                    <span class="project__info-size"><?= $item['description'] ?> <sub>2</sub></span>
                    <p class="project__info-title"><?= $item['title'] ?></p>
                    <a href="<?= \yii\helpers\Url::to("@web/project/" . $item['slug']) ?>" class="project__info-link">
                        <span class=" btn">
                            <b>смотреть проект</b>
                            <i class="icon"><svg><use xlink:href="#icon_search"></use></svg></i>
                        </span>
                    </a>
                </div>
		        <?php $count++ ?>
				<?php $i++; ?>
            <?php endforeach; ?>
			
				 <!-- <div class="  project__info-small  Doma_i_taunhausy">
				<p style="
    position: relative;
    margin-top: -80px;
    text-align: center;
    width: 190px;
    margin-left: 483px;
">Раздел в стадии разработки</p>
                
            </div> -->
			
			
			
			
			
			
			
			  <div class="project__info project__info-small project__info-more  Kvartiry">
<img class="project__info-img" src="assets/img/project/1.jpg" alt="">
                <span class="project__info-size">45м <sub>2</sub></span>
                <p class="project__info-title">Квартира с историей</p>
                <a href="javascript:;" class="project__info-link">
                    <span class=" btn">
                        <b>смотреть проект</b>
                        <i class="icon"><svg><use xlink:href="#icon_search"></use></svg></i>
                    </span>
                </a>
            </div>
            <div class="project__info project__info-small project__info-more  Kvartiry">
<img class="project__info-img" src="assets/img/project/2.jpg" alt="">
                <span class="project__info-size">45м <sub>2</sub></span>
                <p class="project__info-title">Лаконичная классика</p>
                <a href="javascript:;" class="project__info-link">
                    <span class=" btn">
                        <b>смотреть проект</b>
                        <i class="icon"><svg><use xlink:href="#icon_search"></use></svg></i>
                    </span>
                </a>
            </div>
            <div class="project__info project__info-small project__info-more  Kvartiry ">
<img class="project__info-img" src="assets/img/project/3.jpg" alt="">
                <span class="project__info-size">45м <sub>2</sub></span>
                <p class="project__info-title">Геометрия жизни</p>
                <a href="javascript:;" class="project__info-link">
                    <span class=" btn">
                        <b>смотреть проект</b>
                        <i class="icon"><svg><use xlink:href="#icon_search"></use></svg></i>
                    </span>
                </a>
            </div>
    <?php if($cou>0): ?>
            <a href="<?= \yii\helpers\Url::to(Yii::getAlias('@web') . '/portfolio') ?>" class="show_more">
                <span>
                    Показать еще
                </span>
            </a>
			<?php endif;?>
			
        </div>
    </div>
<?php endif; ?>