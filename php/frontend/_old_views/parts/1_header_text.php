<?php
/**
Header for all pages
 */

use yii\helpers\Url;

?>

<div id="header-text">
  <div class="container">
    <div class="row">
      <div class="col-xs-10 col-xs-offset-1">
        <div class="text-left">
          <div class="front-img">
            <h1>Создаем<br/>пространства<br/>для жизни</h1>
          </div>
          <br>
          <p class="header__text">«Студия 3.14» — дизайн интерьера квартир, таунхаусов и&nbsp;домов в&nbsp;Москве, Подмосковье и&nbsp;других регионах</p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-10 col-xs-offset-1">
        <div class="row">
          <div class="col-xs-2 col-xs-employee">
            <div class="fact">
              <span class="fact-num">3</span><br>года
            </div>
          </div>
          <div class="col-xs-2 col-xs-employee">
            <div class="fact">
              <span class="fact-num">90</span><br>проектов
            </div>
          </div>
          <div class="col-xs-2 col-xs-employee">
            <div class="fact">
              <span class="fact-num">13</span><br>домов
            </div>
          </div>
          <div class="col-xs-2 col-xs-employee">
            <div class="fact">
              <span class="fact-num">16</span><br>реализаций<br>за&nbsp;2&nbsp;года
            </div>
          </div>
          <div class="col-xs-2 col-xs-employee">
            <div class="fact">
              <span class="fact-num">4</span><br>тарифа
            </div>
          </div>
        </div>
      </div>
    </div>
    <br>
    <br>
  </div>
</div>

