<div id="press" class="press" style="margin-bottom:40px;">
  <div class="container">
      <div class="row">
        <div class="col-xs-10 col-xs-offset-1">
          <h1>ПРЕССА О НАС</h1>
          <div class="row">
            <div class="col-xs-12 col-sm-4">
              <img src="<?= \yii\helpers\Url::to("@web/img/old_template_img/press/realty.jpg") ?>"><br></br>
              <a class="red-link" target="_blank" href="http://realty.dmir.ru/articles/37732/">Организация интерьера «малогабариток»</a><br></br>
              <p>Стили дизайна в небольших квартирах могут быть разными, главное — их функциональность. Алена Горшкова, дизайнер-архитектор «Студии 3.14», советует уделить основное внимание использованию свободного пространства и зонированию помещений.</p>
            </div>
            <div class="col-xs-12 col-sm-4">
              <img src="<?= \yii\helpers\Url::to("@web/img/old_template_img/press/inmyroom.png") ?>"><br></br>
              <a class="red-link" target="_blank" href="https://www.inmyroom.ru/posts/13664-proekt-nedeli-uyutnaya-kvartira-v-stalinke-s-mebelyu-ikea">Проект недели на InMyRoom</a><br></br>
              <p>В этой сталинке будет жить уже третье поколение семьи – и, конечно, у молодых хозяев была мечта сделать интерьер более современным и удобным. Дизайнеры из «Студии 3.14» воплотили ее в реальность.</p>
            </div>
            <div class="col-xs-12 col-sm-4">
              <img src="<?= \yii\helpers\Url::to("@web/img/old_template_img/press/homify.png") ?>"><br></br>
              <a class="red-link" target="_blank" href="https://www.homify.ru/knigi-idej/38009/skandinavskiy-romans-na-leninskom-prospekte">Скандинавия на Ленинском проспекте</a><br></br>
              <p>Светлая и изящная квартира в скандинавском стиле стала новым проектом дизайнеров из команды «Студия 3.14». Элегантное решение для молодой семьи с ребенком было найдено в светлой цветовой гамме, легком минимализме и нескольких цветовых акцентах.</p>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-4">
              <img src="<?= \yii\helpers\Url::to("@web/img/old_template_img/press/inmyroom.png") ?>"><br></br>
              <a class="red-link" target="_blank" href="https://www.inmyroom.ru/posts/13451-proekt-nedeli-bezhevaya-kvartira-s-yarkimi-akcentami">Проект недели на InMyRoom</a><br></br>
              <p>Кессонный потолок в гостиной, деревянные обои в ТВ-зоне, узоры из декоративных молдингов на стенах – хозяева этой просторной квартиры открыты для интересных дизайнерских решений</p>
            </div>
            <div class="col-xs-12 col-sm-4">
              <img src="<?= \yii\helpers\Url::to("@web/img/old_template_img/press/realty.jpg") ?>"><br></br>
              <a class="red-link" target="_blank" href="http://realty.dmir.ru/articles/36578/">Перепланировка для жизни</a><br></br>
              <p>В жилом фонде осталось немало хрущевок и брежневок, планировки которых зачастую оставляют желать лучшего. Специалисты «Студии 3.14» предлагают варианты их использования.</p>
            </div>
            <div class="col-xs-12 col-sm-4">
              <img src="<?= \yii\helpers\Url::to("@web/img/old_template_img/press/moskva.jpg") ?>"><br></br>
              <a class="red-link" target="_blank" href="http://vm.ru/news/2016/08/15/sekonomit-prostranstvo-pomogut-dvuhetazhnie-divani-329934.html">Экономим пространство</a><br></br>
              <p>Сейчас в дизайне очень востребован так называемый скандинавский стиль. Это недорогая практичная мебель, минимум украшений, максимум функциональности, — рассказывает руководитель «Студии 3.14» Тимур Абдрахманов.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
