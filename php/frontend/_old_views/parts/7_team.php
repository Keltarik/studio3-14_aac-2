<div id="team" class="team">
	<div class="container">
		<div class="row">
			<div class="col-xs-offset-1 col-xs-10">
				<h1>Наша команда</h1>

				<div class="row">
                    <div class="col-xs-2 col-xs-employee">
                        <div class="employee employee__new center">
                            <div class="employee-photo"><a class="red-link" href="<?= \yii\helpers\Url::to('@web/about') ?>">О «Студии
                                    3.14»</a></div>
                            <div class="employee-name"></div>
                            <div class="employee-prof"></div>
                        </div>
                    </div>
					<?php
					/**
					 * @var $team \common\models\type\Teammate[]
					 */
					$team = \common\models\type\Teammate::find()->orderBy('position')->where(['main' => 1])->all();
					foreach ($team as $person) {
						?>
						<div class="col-xs-2 col-xs-employee">
							<a class="js-person"
								 href="<?= \yii\helpers\Url::to('@web/person/' . $person->getAttribute('id')) ?>">
								<div class="employee">
									<div style="background-image: url(<?= $person->getPhotoUrl() ?>)"
											 class="employee-photo"></div>
									<div class="employee-name"><span><?= $person->getAttribute('name') ?></span></div>
									<div class="employee-prof"><span><?= $person->getAttribute('profession') ?></span></div>
								</div>
							</a>
						</div>
						<?php
					}
					?>



					<div class="col-xs-2 col-xs-employee">
						<div class="employee employee__new">
							<div class="employee-photo"><a class="red-link" href="mailto:info@studio3-14.ru">Присоединиться к&nbsp;нашей команде</a></div>
							<div class="employee-name"></div>
							<div class="employee-prof"></div>
						</div>
					</div>
				</div>
				<div class="signature text-center signature-bottom">Хотите
					познакомиться поближе? Добавляйтесь в&nbsp;друзья!
				</div>
				<div class="social text-center">
				<a target="_blank" href="https://www.facebook.com/Place4life/" class="social-icon social-icon-fb" onclick="yaCounter24474014.reachGoal('facebook'); ga('send', 'event', 'Goal', 'facebook'); roistat.event.send('facebook');"></a>
				<a target="_blank" href="http://vk.com/place4life" class="social-icon social-icon-vk" onclick="yaCounter24474014.reachGoal('vkcom'); ga('send', 'event', 'Goal', 'vkcom'); roistat.event.send('vkcom');"></a>
				<a target="_blank" href="https://twitter.com/place4life" class="social-icon social-icon-tw" onclick="yaCounter24474014.reachGoal('twitter'); ga('send', 'event', 'Goal', 'twitter'); roistat.event.send('twitter');"></a>
				<a target="_blank" href="http://instagram.com/place4life" class="social-icon social-icon-in" onclick="yaCounter24474014.reachGoal('instagram'); ga('send', 'event', 'Goal', 'instagram'); roistat.event.send('instagram');"></a>
				<a target="_blank" href="https://www.pinterest.com/place4life/" class="social-icon social-icon-pin" onclick="yaCounter24474014.reachGoal('pinterest'); ga('send', 'event', 'Goal', 'pinterest'); roistat.event.send('pinterest');"></a>
				<a target="_blank" href="http://www.houzz.ru/pro/studio314/" class="social-icon social-icon-houzz" onclick="yaCounter24474014.reachGoal('houzz'); ga('send', 'event', 'Goal', 'houzz'); roistat.event.send('houzz');"></a>
				</div>
			</div>
		</div>
	</div>
</div>