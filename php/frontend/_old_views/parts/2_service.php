<!--div id="service" class="service">
	<div class="container">
		<div class="row">
			<div class="col-xs-10 col-xs-offset-1">
				<h1>Услуги</h1>

				<h3>Чаще всего к&nbsp;нам обращаются с&nbsp;такими задачами:</h3>
			</div>
		</div>
	</div>
	<div class="service-calc">
		<div class="container">
			<div class="row">
				<div class="col-xs-4 col-xs-offset-1">
					<ul class="service-calc-select">
						<li data-tab="0" class="calc-select active">
							<span class="link-dashed">«Мне нужен дизайн и&nbsp;только. С&nbsp;остальным разберусь сам»</span>
						</li>
						<li data-tab="1" class="calc-select">
									<span
										class="link-dashed">«Нужна не только дизайн концепция, но&nbsp;и&nbsp;чертежи для строителей»</span>
						</li>
						<li data-tab="2" class="calc-select">
							<span class="link-dashed">«Нужен дизайн-проект, который помог&nbsp;бы сэкономить время и&nbsp;деньги при&nbsp;покупке материалов и мебели»</span>
						</li>
						<li data-tab="3" class="calc-select">
							<span class="link-dashed">«Доверюсь полностью профессионалам: сделайте и&nbsp;дизайн-проект, и&nbsp;последующий ремонт под ключ!»</span>
						</li>
						<li data-tab="form" class="calc-select">
<span class="link-dashed">Затрудняюсь с&nbsp;формулировкой своих задач
<span class="text-bold">Получить подробную консультацию</span>
</span>
						</li>
					</ul>
				</div>
				<div class="col-xs-6">
					<div class="service-calc-result">
						<div style="top: 29px;" class="calc-result-anchor"></div>
						<h4 class="service-calc-result-p text-gray text-center">Для вас
							подойдет пакет услуг
							<span class="service-calc-result-type text-black text-bold">«Минимальный»</span>
						</h4>
						<ul class="service-calc-result-p calc-result-list">
							<li class="active calc-result-list-item">
								<span class="text-bold">Обмерочный план</span> обеспечит
								точность всех последующих работ
							</li>
							<li class="active calc-result-list-item">
								<span class="text-bold">Планировочные решения с&nbsp;расстановкой мебели</span>
								помогут определиться с&nbsp;оптимальным зонированием пространства
								в&nbsp;вашем доме
							</li>
							<li class="active calc-result-list-item">
								<span class="text-bold">Эскиз проекта</span> даст
								представление о&nbsp;том, как могут быть визуально воплощены ваши
								пожелания
							</li>
							<li class="active calc-result-list-item">
								<span class="text-bold">3D&nbsp;визуализация интерьера</span>
								покажет, как будет выглядеть ваша квартира после ремонта
							</li>
							<li class="calc-result-list-item">
								<span class="text-bold">Комплект строительных чертежей</span>
								обеспечит техническую точность и&nbsp;надежность
							</li>
							<li class="calc-result-list-item">
								<span class="text-bold">Подбор мебели</span> сэкономит время
								на&nbsp;поиск конкретных объектов интерьера и&nbsp;позволит уложиться
								в&nbsp;бюджет
							</li>
							<li class="calc-result-list-item">
								<span class="text-bold">Спецификация материалов и&nbsp;смета</span>
								станет важнейшим инструментом для грамотного, оптимального и
								экономически оправданного подбора материалов
							</li>
							<li class="calc-result-list-item">
								<span class="text-bold">Авторский надзор</span> гарантирует,
								что вы&nbsp;получите квартиру в&nbsp;точности, как мечтали
							</li>
						</ul>
						<div class="service-calc-result-p text-center">
							<a href="<?= \yii\helpers\Url::to("@web/pdf/minimal.pdf") ?>" target="_blank"
								 class="calc-result-link">
								<img src="<?= \yii\helpers\Url::to("@web/img/old_template_img/2/icon-pdf.png") ?>"/>
								<span>Пример дизайн-проекта <span class="service-calc-result-type">«Минимальный»</span></span>
								<span class="anti-link">(pdf 9Мб)</span>
							</a>
						</div>
						<div class="service-calc-result-form form">
							<form action="<?= \yii\helpers\Url::to("@web/consult") ?>" method="post">
								<input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
								<h4 class="form-title text-bold text-center">Заявка на
									консультацию</h4>

								<div class="form-message text-center">Остались вопросы и
									требуется наша консультация?
									<br/>Оставьте ваши контакты, и&nbsp;мы&nbsp;обязательно свяжемся с
									вами в&nbsp;ближайшее время!
								</div>
								<div class="form-input">
									<div class="row">
										<div class="col-xs-4">
											<div class="form-group">
												<input type="text" name="AddReport[user_name]"
															 placeholder="Имя" class="form-control">
											</div>
										</div>
										<div class="col-xs-4">
											<div class="form-group">
												<input type="text" name="AddReport[user_phone]"
															 placeholder="Телефон" class="form-control">
											</div>
										</div>
										<div class="col-xs-4">
											<div class="form-group">
												<input type="email" name="AddReport[user_email]"
															 placeholder="E-mail" class="form-control">
											</div>
										</div>
									</div>
									<div class="form-group">
                        <textarea rows="4" name="AddReport[user_text]"
																	placeholder="Комментарий (необязательно)"
																	class="form-control"></textarea>
									</div>
								</div>
								<div class="text-center">
									<input type="submit" href="#modal_form"
												 value="Оставить заявку"
												 class="btn btn-danger btn-lg form-submit">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-10 col-xs-offset-1">
					<div class="signature signature-bottom">Все эти услуги мы
						предоставляем как в&nbsp;комплексе, так и&nbsp;отдельно. А&nbsp;еще мы
						занимается декорироваем помещений, художественной росписью стен,
						консультируем на&nbsp;этапе приобретения недвижимости о&nbsp;возможностях
						переустройства и&nbsp;использования. При этом мы&nbsp;легко общаемся с
						иностранными заказчиками на&nbsp;английском языке (English friendly).
						<a href="/old/en/index.html" class="signature-english red-link">View in
							english!</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div-->