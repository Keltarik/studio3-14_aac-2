<?php
/**
Header for all pages
 */

use yii\helpers\Url;

?>

<div class="header header__page">
    <nav class="navbar header-nav">
        <div class="container">
            <div class="navbar-header">
                <a href="<?= Url::to("@web/") ?>">
                    <div class="navbar-brand">
                        <img src="<?= Url::to("@web/img/old_template_img/0/logo-big.png") ?>"
                             class="header-nav-logo header-nav-logo__big"/>
                        <img src="<?= Url::to("@web/img/old_template_img/0/logo-small.png") ?>"
                             class="header-nav-logo header-nav-logo__small"/>
                    </div>
                </a>
                <div class="mobile-menu">
                </div>
            </div>
            <ul class="nav navbar-nav">
                <li><a href="<?= Url::to("@web/") ?>#portfolio" id="portfolio-link">Портфолио</a></li>
                <!--li><a href="<?= Url::to("@web/") ?>#service">Услуги</a></li-->
                <li><a href="<?= Url::to("@web/") ?>#price">Цены</a></li>
                <li><a href="<?= Url::to("@web/") ?>#team">О нас</a></li>
                <li><a href="<?= Url::to("@web/") ?>#contacts">Контакты</a></li>
                <li><a href="<?= Url::to("@web/partners") ?>">Партнеры</a></li>
                <li><a href="http://blog.studio3-14.ru">Блог</a></li>
            </ul>
            <div class="nav navbar-right">
                <div class="header-nav-right"><span class="header-nav-right-tel" onclick="yaCounter24474014.reachGoal('phone'); ga('send', 'event', 'Goal', 'phone'); roistat.event.send('phone');"><span class="roistat-phone">+7 499 398-17-33</span></span>
                    <!--a href="/old/en/index.html" class="btn btn-default btn-sm">english</a-->
                </div>
            </div>
        </div>
    </nav>
</div>
