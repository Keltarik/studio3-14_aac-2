<?php
/**
 */
use yii\helpers\Url;
$this->registerJsFile(Url::to("@web/assets/jquery.min.js"), ['position' => $this::POS_END]);
$this->title = 'О &laquo;Студии&nbsp;3.14&raquo;';
$access_token="1523133044.3e9d51f.d02faa885b874604b2f9282cd1996a83";
$photo_count=8;

$json_link="https://api.instagram.com/v1/users/self/media/recent/?";
$json_link.="access_token={$access_token}&count={$photo_count}";

$json = file_get_contents($json_link);
$obj = json_decode($json, true, 512, JSON_BIGINT_AS_STRING);
?>

<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.5&appId=1507789006102182";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<div class="wrapper">
	<div style="background-image:url(<?= Url::to('@web/img/old_template_img/people/2016_blur.jpg') ?>)" class="wrapper-img"></div>

	<div class="wrapper-back">

		<div class="page_head">
			<div class="container">
				<div class="page_head-content">
					<div style="height:675px;background-image:url(<?= Url::to('@web/img/old_template_img/people/2016.jpg') ?>)"
							 class="page_head-content-img"></div>
					<div class="page_head-content-title col-xs-offset-1"><h1></h1></div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-xs-offset-11 col-xs-1 site-button">


					<div class="site-button-over"><a href="#consulting" class="js-scroll-link site-button-link">Заказать<br>дизайн</a></div>
				</div>
			</div>
		</div>
		<div class="container page-content">
			<div class="row">
				<div class="col-xs-offset-1 col-xs-10">
					<div class="page_content">
						<h2 class="text-uppercase"></h2></div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-10 col-xs-offset-1">
						<p class="page__text">«Студия&nbsp;3.14»&nbsp;— творческая команда дизайнеров, архитекторов, визуализаторов, объединившаяся в&nbsp;2013 году для создания «под ключ» интерьеров комфорт и&nbsp;бизнес-класса.</p>

						<p class="page__text">Студия работает в&nbsp;Москве, Московской области и&nbsp;регионах России и&nbsp;специализируется на&nbsp;создании частных интерьеров: квартир, таунхаусов и&nbsp;загородных домов. Помимо тщательной проработки дизайн-концепции студия воплощает в&nbsp;жизнь проектные решения, помогая своим клиентам в&nbsp;сопровождении строительных работ, комплектации и&nbsp;декорировании интерьеров.</p>

                        <p class="page__text">Кредо «Студии&nbsp;3.14»&nbsp;— создавать пространства для жизни.</p>

						<p class="page__text">Для нас главную роль в&nbsp;доме играют люди, а&nbsp;не&nbsp;вещи, и&nbsp;дизайн интерьера не&nbsp;сводится к&nbsp;планировке помещений, размещению мебели и&nbsp;подбору элементов декора. Команда студии создает интерьеры, которые соответствуют характеру хозяев и&nbsp;отражают их&nbsp;стиль жизни,&nbsp;— такие пространства, в&nbsp;которые приятно возвращаться каждый день и&nbsp;чувствовать себя комфортно.</p>

						<p class="page__text">Мы&nbsp;убеждены, что в&nbsp;идеальном интерьере гармония достигается не&nbsp;только за&nbsp;счет внешних форм, но&nbsp;и&nbsp;благодаря тому, что учтены функциональные связи помещений, и&nbsp;каждый предмет расположен на&nbsp;своем месте. В&nbsp;своих проектах мы&nbsp;уделяем максимум внимания тому, чтобы красота сочеталась с&nbsp;практичностью, естественностью и&nbsp;комфортом.</p>

						<p class="page__text">Надежность и&nbsp;долговечность в&nbsp;дизайне интерьера играют далеко не&nbsp;последнюю роль. Разработать проект, реализуемый на&nbsp;практике, и&nbsp;соблюсти технические нормы&nbsp;— обязательное условие работы «Студии&nbsp;3.14». Опыт и&nbsp;квалификация сотрудников студии позволяют нам следовать этому правилу.</p>

						<p class="page__text">Учёт потребностей клиента, практичность и&nbsp;качество&nbsp;— те&nbsp;принципы, которыми мы&nbsp;руководствуемся в&nbsp;каждом проекте. Результат&nbsp;— дом, в&nbsp;который приятно возвращаться каждый день и&nbsp;чувствовать себя комфортно.</p>

						<p class="page__text">Если вы&nbsp;в&nbsp;начале пути к&nbsp;дому своей мечты&nbsp;— позвоните нам,&nbsp;— мы&nbsp;знаем туда дорогу.</p>
				</div>
			</div>


			<div class="row">
				<div class="col-xs-10 col-xs-offset-1">
						<h3>О нас пишут</h3>

                        <img style="margin-bottom: 10px;" src="/img/old_template_img/about/inmyroom.png"/>
                        <p class="page__text">
                            <a target="_blank"
                               href="http://www.inmyroom.ru/posts/12304-kak-sochetat-yarkie-tsveta-v-interiere-proekt-doma-ot-studii-3-14"
                               class="red-link md2">Проект недели: Как сочетать яркие цвета в интерьере: проект дома от «Студии 3.14»</a>
                        </p>
                        <p class="page__text">
                            <a target="_blank"
                               href="http://www.inmyroom.ru/posts/12217-proekt-nedeli-uyutnaya-kvartira-studiya-s-retrostenkoy"
                               class="red-link md2">Проект недели: уютная квартира-студия с ретростенкой</a>
                        </p>
                        <p class="page__text">
                            <a target="_blank"
                               href="http://www.inmyroom.ru/posts/11953-proekt-nedeli-svetlaya-treshka-s-etnicheskimi-motivami-v-himkah"
                               class="red-link md2">Проект недели: светлая трешка с этническими мотивами в Химках</a>
                        </p>
                        <p class="page__text">
                            <a target="_blank"
                               href="http://www.inmyroom.ru/posts/11594-proekt-nedeli-sovremennaya-kvartira-v-istoricheskom-rayone-moskvy"
                               class="red-link md2">Проект недели: современная квартира в историческом районе Москвы</a>
                        </p>
                        <p class="page__text">
                            <a target="_blank"
                               href="http://www.inmyroom.ru/posts/11414-proekt-nedeli-kak-oformit-tipovuyu-kvartiru-dlya-mnogodetnoy-semi"
                               class="red-link md2">Проект недели: как оформить типовую квартиру для многодетной семьи</a>
                        </p>
						<p class="page__text">
							<a target="_blank"
								 href="http://www.inmyroom.ru/posts/10598-proekt-nedeli-kak-oformit-treshku-v-brezhnevke"
								 class="red-link md2">Проект недели: как оформить трешку в брежневке</a>

						</p>
                        <img style="margin-bottom: 10px;" src="/img/old_template_img/about/homify-black.png"/>
                        <p class="page__text">
							<a target="_blank"
								 href="https://www.homify.ru/knigi-idej/38009/skandinavskiy-romans-na-leninskom-prospekte"
								 class="red-link md2">Скандинавский романс на Ленинском проспекте</a>
						</p>
						<p class="page__text">
							<a target="_blank" href="https://www.homify.ru/knigi-idej/28089/kak-vybrat-mebel-dlya-gostinoy"
								 class="red-link md2">Как выбрать мебель для гостиной</a>
						</p>
						<p class="page__text">
							<a target="_blank" href="https://www.homify.ru/knigi-idej/27476/zonirovanie-komnaty-s-pomoschyu-polov"
								 class="red-link md2">Зонирование комнаты с помощью полов</a>
						</p>
						<p class="page__text">
							<a target="_blank" href="https://www.homify.ru/knigi-idej/24905/6-ocharovatelnyh-detskih-komnat"
								 class="red-link md2">6 очаровательных детских комнат</a>
						</p>
						
				</div>
			</div>
			<div class="row">
				<div class="col-xs-10 col-xs-offset-1">
					<h1>Наша команда</h1>

					<div class="row">
	                    
					<?php
					/**
					 * @var $team \common\models\type\Teammate[]
					 */
					$team = \common\models\type\Teammate::find()->orderBy('position')->where(['past' => 0])->all();
					foreach ($team as $person) {
						?>
						<div class="col-sm-2 col-xs-12 col-xs-employee">
							<a class="js-person"
								 href="<?= \yii\helpers\Url::to('@web/person/' . $person->getAttribute('id')) ?>">
								<div class="employee">
									<div style="background-image: url(<?= $person->getPhotoUrl() ?>)"
											 class="employee-photo"></div>
									<div class="employee-name"><span><?= $person->getAttribute('name') ?></span></div>
									<div class="employee-prof"><span><?= $person->getAttribute('profession') ?></span></div>
								</div>
							</a>
						</div>
						<?php
					}
					?>
	
					</div>
				</div>
			</div>
            <div class="row">
                <div class="col-sm-5 col-xs-offset-1 col-xs-10">

                    <br/>
                    <h3>Facebook</h3>
                    <div class="fb-page" data-href="https://www.facebook.com/Place4life/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-height="705"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/Place4life/"><a href="https://www.facebook.com/Place4life/">Студия 3.14</a></blockquote></div></div>

                </div>
                <div class="col-sm-5 col-xs-offset-1 col-xs-10">

                    <br/>
                    <h3>Instagram</h3>
                    <?php
                    foreach ($obj['data'] as $post) {

                        $pic_text=$post['caption']['text'];
                        $pic_link=$post['link'];
                        $pic_src=str_replace("http://", "https://", $post['images']['low_resolution']['url']);

                        echo "<div class='item_box'>";
                        echo "<a href='{$pic_link}' target='_blank'>";
                        echo "<img class='img-responsive photo-thumb' src='{$pic_src}' alt='{$pic_text}'>";
                        echo "</a>";
                        echo "</div>";
                    }
                    ?>
                </div>
            </div>

			<div id="consulting" class="container">
				<div class="row">
					<div class="col-xs-offset-1 col-xs-10">
						<div class="form">
							<form action="<?= \yii\helpers\Url::to("@web/consult") ?>" method="post">
								<h4 class="form-title text-bold text-center">Заявка на бесплатную консультацию</h4>


								<input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken() ?>"/>

								<div class="form-message text-center">Оставьте заявку на бесплатную консультацию прямо сейчас <br/>и мы свяжемся с вами в течение дня
								</div>
								<div class="form-input">
									<div class="row">
										<div class="col-sm-6 col-xs-12">
											<div class="form-group"><input type="text" name="AddReport[user_name]" placeholder="Имя"
																										 class="form-control"></div>
										</div>
										<div class="col-sm-6 col-xs-12">
											<div class="form-group"><input type="text" name="AddReport[user_phone]" placeholder="Телефон"
																										 class="form-control"></div>
										</div>
									</div>
                                    <div class="row form-email">
                                        <div class="col-xs-12">
                                            <div class="form-group"><input tabindex="-1" type="email"
                                                                           name="AddReport[user_email]"
                                                                           placeholder="E-mail"
                                                                           class="form-control"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <textarea rows="4" name="AddReport[user_text]" placeholder="Комментарий (необязательно)"
                                                          class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>
								</div>
								<div class="text-center"><input type="submit" value="Отправить заявку"
																								class="btn btn-danger btn-lg form-submit">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>

            <div class="row">
                <div class="col-xs-10 col-xs-offset-1">
                    <div class="social text-center">
                        <a target="_blank" href="https://www.facebook.com/place4life" class="social-icon social-icon-fb"></a>
                        <a target="_blank" href="https://vk.com/place4life" class="social-icon social-icon-vk"></a>
                        <a target="_blank" href="https://twitter.com/place4life" class="social-icon social-icon-tw"></a>
                        <a target="_blank" href="http://instagram.com/place4life" class="social-icon social-icon-in"></a>
                        <a target="_blank" href="http://www.pinterest.com/place4life" class="social-icon social-icon-pin"></a>
                        <a target="_blank" href="http://www.houzz.ru/pro/studio314/" class="social-icon social-icon-houzz"></a>
                    </div>
                </div>
            </div>


		</div>
	</div>
</div>
