<?php
/**
 * @var $project \common\models\type\Partner
 */
use yii\helpers\Url;
$this->title = 'Партнеры &laquo;Студии&nbsp;3.14&raquo;';
$this->registerJsFile(Url::to("@web/assets/jquery.min.js"), ['position' => $this::POS_END]);
?>

<div class="wrapper">
  <div style="background-image:url(/img/old_template_img/section/partners_blur.jpg)" class="wrapper-img"></div>
  <div class="wrapper-back">
    <div class="page_head">
      <div class="container">
        <div class="page_head-content">
          <div style="background-image:url(/img/old_template_img/section/partners.jpg)"
               class="page_head-content-img"></div>
          <div class="page_head-content-title col-xs-offset-1">
            <h1></h1>
          </div>
        </div>
      </div>
    </div>
    <div class="container page-content">
      <div class="row">
        <div class="col-xs-offset-1 col-xs-10">
          <div class="page_content">
            <h2 class="text-uppercase">Партнеры</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-offset-1 col-xs-10">
          <div class="page_content page__text">
            <p>Для нас важно, чтобы у&nbsp;наших клиентов был самый широкий выбор возможностей в&nbsp;создании интерьеров свой мечты, также важно качество товаров, надежность и&nbsp;порядочность компаний, которые оказывают свои услуги. Именно поэтому &laquo;Студия&nbsp;3.14&raquo; непрерывно изучает рынок производителей и&nbsp;поставщиков отделочных материалов, мебели и&nbsp;предметов декора и&nbsp;развивает партнерскую базу компаний, продукцию которых использует в&nbsp;своих проектах и&nbsp;рекомендует клиентам. Сегодня мы&nbsp;активно работаем более чем со&nbsp;170&nbsp;компаниями, в&nbsp;числе которых:</p>

             <ul class="projects">
            <?php
            /**
             * @var $team \common\models\type\Partner[]
             */
            $partner = \common\models\type\Partner::find()->where(['visible' => 1])->all();
            foreach ($partner as $pa) {
				?>
                <li>
                    <a class="js-person" style="text-decoration: none;outline: none;border-bottom: none;"
                       href="<?= \yii\helpers\Url::to('@web/partner/' . $pa->getAttribute('id')) ?>">
                        <span><img src="<?= $pa->getLogoUrl(); ?>"/></span>
                    </a>
                </li>
                <?php
            }
            ?>
            </ul>

            <p>Уважаемые производители и&nbsp;поставщики, если вы&nbsp;заинтересованы в&nbsp;сотрудничестве с&nbsp;нами, присылайте ваши предложения на адрес <a class="red-link" href="mailto:partner@studio3-14.ru">partner@studio3-14.ru</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


