<?php
/**
 * Created by PhpStorm.
 * User: macos
 * Date: 08.10.17
 * Time: 19:36
 */

namespace frontend\models\form;


use common\models\type\Report;
use yii\base\Model;

class CallbackForm extends Model
{
    public $name;
    public $telephone;
    public $email;

    public function rules()
    {
        return [
            [['name', 'telephone', 'email'], 'required'],
            [['name'], 'string', 'max' => 30, 'min' => 3],
            [['telephone'], 'string', 'max' => 15],
            [['email'], 'email']
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'telephone' => 'Номер телефона',
            'email' => 'E-mail'
        ];
    }

    public function send()
    {
        if (!$this->validate()) {
            return false;
        }

        $model = new Report();

        $model->setAttributes([
            'user_name' => $this->name,
            'user_email' => $this->email,
            'user_phone' => $this->telephone
        ]);

        if ($model->save()) {
            return true;
        }

        return false;
    }
}