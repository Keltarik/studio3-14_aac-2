<?php
/**
 * Created by PhpStorm.
 * User: zachot
 * Date: 05.08.15
 * Time: 21:33
 */

namespace frontend\models\type;


use yii\helpers\Url;

class Project extends \common\models\type\Project {
	/**
	 * @return string
	 */
	public function getUrl() {
		return Url::to('@web/project/' . $this->getAttribute('slug'));
	}


}