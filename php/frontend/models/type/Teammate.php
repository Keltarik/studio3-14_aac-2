<?php
/**
 * Created by PhpStorm.
 * User: zachot
 * Date: 05.08.15
 * Time: 18:42
 */

namespace frontend\models\type;


class Teammate extends \common\models\type\Teammate {
	/**
	 * @return Project[]
	 */
	function getProjects() {
		return Project::find()
			->where('FIND_IN_SET(' . $this->getAttribute('id') . ',`team-ids`)')
            ->orderBy('id desc')
			->all();
	}
}