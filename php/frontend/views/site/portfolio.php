<?php
use yii\helpers\Url;
$this->title = 'Все работы &laquo;Студии&nbsp;3.14&raquo;';
$this->registerJsFile('/js/portfolio-full.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="wrapper">
  <div style="background-image:url(/img/old_template_img/portfolio/portfolio_blur.jpg)" class="wrapper-img"></div>
  <div class="wrapper-back">
    <div class="page_head">
      <div class="container">
        <div class="page_head-content">
          <div style="background-image:url(/img/old_template_img/portfolio/portfolio.jpg)"
               class="page_head-content-img"></div>
          <div class="page_head-content-title col-xs-offset-1">
            <h1></h1>
          </div>
        </div>
      </div>
    </div>
    <div class="container page-content">
      <div class="row">
        <br/>
        <br/>
        <br/>
      </div>
        <div id="portfolio" class="portfolio">
                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1" id="items-container"><h1 class="first-on-page">Наши проекты</h1>

                        <div>
                            <ul class="nav portfolio-tags">
                                <?php foreach (\common\models\type\Tag::find()->all() as $tag):?>
                                    <li><a class="tag-filter-link" href="#portfolio<?=$tag->slug?>" data-id-slug="<?=$tag->slug?>"><?=$tag->name?></a></li>
                                <?php endforeach?>
                                <li><a href="#portfolio" id="clear-tags" style="display: none">x</a></li>
                            </ul>
                        </div>
                        <div class="portfolio-gallery">

                            <div class="row view">
                                <?php
                                $string = file_get_contents(__DIR__ . "/../../../../settings/portfolio.json");
                                $json = json_decode($string, true);
                                //				print_r($json);
                                $size_counter = 0;
                                foreach ($json as $pr) {

                                    if (!isset($pr['size']))
                                        continue;

                                    $pr['image_url'] = (isset($pr['image_url'])) ? $pr['image_url'] : "";
                                    $pr['slug'] = (isset($pr['slug'])) ? $pr['slug'] : "";
                                    $pr['title'] = (isset($pr['title'])) ? $pr['title'] : "";
                                    $pr['description'] = (isset($pr['description'])) ? $pr['description'] : "";
                                    $pr['tag'] = (isset($pr['tag'])) ? $pr['tag'] : "";
                                    $pr['tag_slug'] = (isset($pr['tag_slug'])) ? $pr['tag_slug'] : "";

                                    if (($size_counter > 0) && ($size_counter >= 12)) {
                                        $size_counter = 0;
                                        echo '</div><div class="row view">';
                                    }
                                    ?>
                                    <div class="col-xs-12 col-sm-<?= $pr['size'] ?>">
                                        <div style="background-image: url(<?= $pr["image_url"] ?>)"
                                             class="portfolio-gallery-thumbnail" data-tag="<?= $pr['tag'] ?>" data-tag-slug="<?= $pr['tag_slug'] ?>"><a
                                                href="<?= \yii\helpers\Url::to("@web/project/" . $pr['slug']) ?>"><span><?= $pr["title"] ?></span><span
                                                    class="area"><?= $pr["description"] ?></span></a>
                                        </div>
                                    </div>
                                    <?php
                                    $size_counter += intval($pr['size']);
                                }
                                ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-xs-1 site-button">
                        <div class="site-button-over">
                            <a href="#consulting" class="js-scroll-link site-button-link" id="consulting-red-button">Заказать &nbsp;дизайн</a>
                        </div>
                    </div>
                </div>
          </div>
        <?=$this->render("@app/_old_views/parts/8_form.php")?>
    </div>
  </div>


</div>
