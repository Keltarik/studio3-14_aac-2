<?php
/**
 * Created by PhpStorm.
 * User: zachot
 * Date: 05.08.15
 * Time: 20:27
 */
$this->title = 'Страница не найдена';
?>
<div class="wrapper">
    <div class="wrapper-back" style="padding-bottom: 0px">

        <div class="page_head">
            <div class="container">
                <div class="page_head-content">
                    <div style="height:475px;background-image:url(<?= \yii\helpers\Url::to('@web/img/old_template_img/404.jpg') ?>)"
                         class="page_head-content-img"></div>
                    <div class="page_head-content-title col-xs-offset-1"><h1></h1></div>
                </div>
            </div>
        </div>
        <div class="headline">
            Страница не найдена. Попробуйте поискать <a href="<?= \yii\helpers\Url::to('@web/') ?>">на главной</a>
        </div>
    </div>
</div>
