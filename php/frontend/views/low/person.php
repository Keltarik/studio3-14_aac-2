<?php
/**
 * @var $person \frontend\models\type\Teammate
 */
?>

<div class="person_popup" style="top: -4.5px;">
	<button title="Close (Esc)" type="button" class="mfp-close"></button>
	<div class=" person_popup-fill">
		<div class="row ">
			<div class="col-sm-3 col-xs-12">
				<div style="background-image: url(<?= $person->getPhotoUrl() ?>);"
						 class="person_popup-photo employee-photo photo"></div>
			</div>
			<div class="col-sm-9 col-xs-12">
				<div class="person_popup-wrapper">
					<h3 class="person_popup-name employee-name"><?= $person->getAttribute('name') ?></h3>

					<div class="person_popup-post employee-prof"><?= $person->getAttribute('profession') ?></div>

					<div class="person_popup-citation"><?= $person->getAttribute('about') ?></div>
					<div class="person_popup-projects">
						<?php
						$projects = ($person->getProjects());

						if ($projects) {
							?>
							<h3>Проекты:</h3>

							<ul>
								<?php foreach ($projects as $project) { ?>
									<li><a href="<?= $project->getUrl() ?>"><?= $project->getAttribute('title') ?></a></li>
									<?php
								} ?>
							</ul>
							<?php
						}
						?>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>