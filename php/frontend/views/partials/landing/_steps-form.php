<?php

/* @var $this \yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="steps wrapper">
    <div class="steps__info">
        <h2 class="steps__title title-line">
            Получите расчет<br>
            вашего проекта<br>
            за 3 минуты
        </h2>
        <p class="steps__description">
            Чтобы его получить, пройдите небольшой тест,<br>
            который покажет стоимость и время реализации<br>
            проекта
        </p>
    </div>
    <div class="steps__form">
        <?= Html::beginForm(\yii\helpers\Url::to(/*"@web/callback"*/ ['/site/callback']), 'POST', ['class' => 'step-by-step-form', 'enctype' => 'multipart/form-data']) ?>
            <div class="form-step">
                <div class="form-step-title">
                    Вам нужен дизайн-проект:
                </div>
                <div class="form-step-radios">
                    <div class="form-step-group" style="margin-right: 133px;">
                        <input type="radio" id="home" name="project__designe" value="Дом"  checked/>
                        <label for="home"><span></span>Дома</label>
                    </div>
                    <div class="form-step-group">
                        <input type="radio" id="apartments" name="project__designe" value="Квартира" />
                        <label for="apartments"><span></span>Квартиры</label>
                    </div>
                </div>
                <div class="step-form-next star_time">
                    <span>Далее</span>
                    <i class="icon"><svg><use xlink:href="#icon_right"></use></svg></i>
                </div>

            </div>
            <div class="form-step">
                <div class="form-step-title">
                    Площадь:
                </div>
                <label for="area_width" class="form-step-input">
                    <input type="text" id="area_width" name="project__area_width" placeholder="Введите">
                    <span>м2</span>
                </label>
                <div class="form-step-title">
                    Комнаты:
                </div>
                <div class="form-step-radios">
                    <div class="form-step-group" style="margin-right: 15px;">
                        <input type="radio" id="studio" name="project__home" value="Студия"  checked/>
                        <label for="studio"><span></span>Студия</label>
                    </div>
                    <div class="form-step-group">
                        <input type="radio" id="studio-1" name="project__home" value="1" />
                        <label for="studio-1"><span></span>1</label>
                    </div>
                    <div class="form-step-group">
                        <input type="radio" id="studio-2" name="project__home" value="2" />
                        <label for="studio-2"><span></span>2</label>
                    </div>
                    <div class="form-step-group">
                        <input type="radio" id="studio-3" name="project__home" value="3" />
                        <label for="studio-3"><span></span>3</label>
                    </div>
                    <div class="form-step-group">
                        <input type="radio" id="studio-4" name="project__home" value="4" />
                        <label for="studio-4"><span></span>4</label>
                    </div>
                    <div class="form-step-group">
                        <input type="radio" id="more" name="project__home" value="Более" />
                        <label for="more"><span></span>Более</label>
                    </div>
                </div>
                <div class="form-step-group">
                    <input type="checkbox" id="checkbox-1-1" class="ui-checkbox" name="project__new_home" value="Новый дом без отделки" checked/>
                    <label for="checkbox-1-1">Новый дом без отделки</label>
                </div>
                <div class="form-step-file">
                    <span>У меня есть исходный план:</span>
                    <div class="file-drop-area">
                        <span class="fake-btn">
                            <img src="<?= Yii::getAlias('@web') ?>/img/icons/file.png" alt="">
                        </span>
                        <span class="file-msg js-set-number">Прикрепить файл</span>
                        <?= Html::fileInput('UploadFileCallback[file]', null, ['class' => 'file-input']) ?>
                    </div>
                </div>
                <div class="step-form-button">
                    <div class="step-form-prev">
                        <i class="icon"><svg><use xlink:href="#icon_left"></use></svg></i>
                        <span>назад</span>
                    </div>
                    <div class="step-form-next">
                        <span>Далее</span>
                        <i class="icon"><svg><use xlink:href="#icon_right"></use></svg></i>
                    </div>
                </div>
            </div>
            <div class="form-step">
                <div class="form-step-title">
                    Состав проживающих:
                </div>
                <label for="sostav_example" class="form-step-input">
                    <input type="text" id="sostav_example" placeholder="Например, муж и жена" name="project__male_female">
                </label>
                <div class="step-form-button">
                    <div class="step-form-prev">
                        <i class="icon"><svg><use xlink:href="#icon_left"></use></svg></i>
                        <span>назад</span>
                    </div>
                    <div class="step-form-next">
                        <span>Далее</span>
                        <i class="icon"><svg><use xlink:href="#icon_right"></use></svg></i>
                    </div>
                </div>
            </div>
            <div class="form-step">
                <div class="form-step-title">
                    Вам нужно:
                </div>
                <div class="form-step-group">
                    <input type="checkbox" id="checkbox-2-1" class="ui-checkbox" name="project__needed[]" value="Планировочное решение"/>
                    <label for="checkbox-2-1">Планировочное решение</label>
                </div>
                <div class="form-step-group">
                    <input type="checkbox" id="checkbox-2-2" class="ui-checkbox" name="project__needed[]" value="Рабочая документация для строителей"/>
                    <label for="checkbox-2-2">Рабочая документация для строителей</label>
                </div>
                <div class="form-step-group">
                    <input type="checkbox" id="checkbox-2-3" class="ui-checkbox" name="project__needed[]" value="Подбор мебели"/>
                    <label for="checkbox-2-3">Подбор мебели</label>
                </div>
                <div class="form-step-group">
                    <input type="checkbox" id="checkbox-2-4" class="ui-checkbox" name="project__needed[]" value="3d визуализация"/>
                    <label for="checkbox-2-4">3d визуализация</label>
                </div>
                <div class="form-step-group">
                    <input type="checkbox" id="checkbox-2-5" class="ui-checkbox" name="project__needed[]" value="Комплектация объекта отделочными материалами"/>
                    <label for="checkbox-2-5">Комплектация объекта отделочными материалами<br>
                        и мебелью</label>
                </div>
                <div class="form-step-group">
                    <input type="checkbox" id="checkbox-2-6" class="ui-checkbox" name="project__needed[]" value="Ведение бюджета"/>
                    <label for="checkbox-2-6">Ведение бюджета</label>
                </div>
                <div class="form-step-group">
                    <input type="checkbox" id="checkbox-2-7" class="ui-checkbox" name="project__needed[]" value="Авторский надзор за ремонтом"/>
                    <label for="checkbox-2-7">Авторский надзор за ремонтом</label>
                </div>
                <div class="form-step-group">
                    <input type="checkbox" id="checkbox-2-8" class="ui-checkbox" name="project__needed[]" value="емонтные работы"/>
                    <label for="checkbox-2-8">Ремонтные работы</label>
                </div>
                <div class="form-step-group">
                    <input type="checkbox" id="checkbox-2-9" class="ui-checkbox" name="project__needed[]" value="Декорирование готового интерьера предметами"/>
                    <label for="checkbox-2-9">Декорирование готового интерьера предметами<br>
                        искусства, домашним текстилем и др.</label>
                </div>
                <div class="step-form-button">
                    <div class="step-form-prev">
                        <i class="icon"><svg><use xlink:href="#icon_left"></use></svg></i>
                        <span>назад</span>
                    </div>
                    <div class="step-form-next">
                        <span>Далее</span>
                        <i class="icon"><svg><use xlink:href="#icon_right"></use></svg></i>
                    </div>
                </div>
            </div>
            <div class="form-step">
                <div class="form-step-title">
                    Стилистически Вы больше тяготеете к:
                </div>
                <div class="form-step-group">
                    <input type="checkbox" id="checkbox-3-1" class="ui-checkbox" name="project__user_style[]" value="Современный стиль"/>
                    <label for="checkbox-3-1">Современный стиль</label>
                </div>
                <div class="form-step-group">
                    <input type="checkbox" id="checkbox-3-2" class="ui-checkbox" name="project__user_style[]" value="Классика"/>
                    <label for="checkbox-3-2">Классика</label>
                </div>
                <div class="form-step-group">
                    <input type="checkbox" id="checkbox-3-3" class="ui-checkbox" name="project__user_style[]" value="Лофт"/>
                    <label for="checkbox-3-3">Лофт</label>
                </div>
                <div class="form-step-group">
                    <input type="checkbox" id="checkbox-3-4" class="ui-checkbox" name="project__user_style[]" value="Скандинавия"/>
                    <label for="checkbox-3-4">Скандинавия</label>
                </div>
                <div class="form-step-group">
                    <input type="checkbox" id="checkbox-3-5" class="ui-checkbox" name="project__user_style[]" value="Минимализм"/>
                    <label for="checkbox-3-5">Минимализм</label>
                </div>
                <div class="form-step-group">
                    <input type="checkbox" id="checkbox-3-6" class="ui-checkbox" name="project__user_style[]" value="Ар-деко"/>
                    <label for="checkbox-3-6">Ар-деко</label>
                </div>
                <div class="form-step-group">
                    <input type="checkbox" id="checkbox-3-7" class="ui-checkbox" name="project__user_style[]" value="Смешение стилей"/>
                    <label for="checkbox-3-7">Смешение стилей</label>
                </div>
                <div class="step-form-button">
                    <div class="step-form-prev">
                        <i class="icon"><svg><use xlink:href="#icon_left"></use></svg></i>
                        <span>назад</span>
                    </div>
                    <div class="step-form-next">
                        <span>Далее</span>
                        <i class="icon"><svg><use xlink:href="#icon_right"></use></svg></i>
                    </div>
                </div>
            </div>
            <div class="form-step">
                <label for="order_name">
                    <i class="icon"><svg><use xlink:href="#icon_person"></use></svg></i>
                    <input type="text" placeholder="Ваше имя" id="order_name" name="project__user_name">
                </label>
                <label for="order_phone">
                    <i class="icon"><svg><use xlink:href="#icon_phone"></use></svg></i>
                    <input type="phone" placeholder="Телефон" id="order_phone" name="project__user_phone">
                </label>
                <label for="order_email">
                    <i class="icon"><svg><use xlink:href="#icon_email"></use></svg></i>
                    <input type="email" placeholder="Электронная почта" id="order_email" name="project__user_email">
                </label>
                <p class="form-step-thanks">
                    Благодарим за экономию нашего времени!<br>
                    Мы приготовили для Вас приятный подарок
                </p>

                <div class="step-form-button">
                    <div class="step-form-prev">
                        <i class="icon"><svg><use xlink:href="#icon_left"></use></svg></i>
                        <span>назад</span>
                    </div>
                    <button type="submit" class="order__form-submit btn step-form-confirm" id="step_submit">
                        <span>Получить расчет</span>
                        <i class="icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon_pen"></use></svg></i>
                    </button>
                </div>
                <p class="order__form-person">
                    Нажимая на кнопку «Оставить заявку»,<br>
                    <span>Вы соглашаетесь на <a href="<?= Yii::getAlias('@web') ?>/pdf/policy.pdf" data-fancybox="">обработку персональных данных.</a></span>
                </p>
            </div>
        <?= Html::endForm() ?>
    </div>
    <div class="steps__gift">
        <div class="steps__gift-info">
            <p>Заполните за <span>3 минуты</span><br>
                и получите авторский надзор<br>
               на 1 месяц
                в подарок</p>
        </div>
        <div class="steps__gift-length">
            <p class="steps__gift-length-info">
                <span class="steps__gift-length-now">0<b>1</b></span>
                <span class="steps__gift-length-total">/ <b>0</b></span>
                <span>вопросов</span>
            </p>
            <div class="steps__gift-length-time">
                <i class="icon"><svg><use xlink:href="#icon_time"></use></svg></i>
                <span id="time">00:03:00</span>
            </div>
        </div>
    </div>
</div>

<?php

$script = <<< JS
    $('.step-by-step-form').on('submit', function () {
        
        var formSteps = $('.step-by-step-form')[0];
        var formDataSteps = new FormData(formSteps);
        
        $.ajax({
            method: 'POST',
            url: $('.step-by-step-form').attr('action'),
            data: formDataSteps,
            contentType: false,
            processData: false,
            cache: false,
            success: function(response) {
                alert('Сообщение отправлено');
            },
            error: function() {
                alert('Возникла ошибка при отправке запроса.');
            }
        });
        
        return false;
    });
JS;

$this->registerJs($script, yii\web\View::POS_END);

?>
