<div class="reward wrapper">
    <h2 class="reward__title title-line">
        Обладатели наград
    </h2>
    <div class="reward__fieldsets">
        <fieldset>
            <legend>Рекомендовано на Houzz</legend>
            <div class="reward__description">
                <img src="<?= Yii::getAlias('@web') ?>/img/reward/1.png" alt="">
                <div class="reward__description-info">
                    <p>
                        Пользователи Houzz рекомендуют<br>
                        этого эксперта.
                    </p>
                    <p>Награжден 10 февраля, 2016</p>
                </div>
            </div>
        </fieldset>
        <fieldset class="fieldsets-right">
            <legend>Лидер мнений Houzz</legend>
            <div class="reward__description">
                <img src="<?= Yii::getAlias('@web') ?>/img/reward/2.png" alt="">
                <div class="reward__description-info">
                    <p>
                        Его профессиональные знания и опыт высоко<br>
                        оценены участниками сообщества Houzz.
                    </p>
                    <p>Награжден 21 июля, 2016</p>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <legend>Best of Houzz 2017 - Клиентский рейтинг</legend>
            <div class="reward__description">
                <img src="<?= Yii::getAlias('@web') ?>/img/reward/3.png" alt="">
                <div class="reward__description-info">
                    <p>
                        Сообщество Houzz оценило этого профессионала<br>
                        за лучшее качество обслуживания клиентов.
                    </p>
                    <p>Награжден 14 января, 2017</p>
                </div>
            </div>
        </fieldset>
        <fieldset class="fieldsets-right">
            <legend>Best of Houzz 2016 - Клиентский рейтинг</legend>
            <div class="reward__description">
                <img src="<?= Yii::getAlias('@web') ?>/img/reward/4.png" alt="">
                <div class="reward__description-info">
                    <p>
                        Сообщество Houzz оценило этого профессионала<br>
                        за лучшее качество обслуживания клиентов.
                    </p>
                    <p>Награжден 16 января, 2016</p>
                </div>
            </div>
        </fieldset>
        <fieldset class="fieldsets-center">
            <legend>Inmyroom.ru: “Лучший 3D-проекта 2016 года: выбор редакции”</legend>
            <div class="reward__description">
                <img src="<?= Yii::getAlias('@web') ?>/img/press/1.png" alt="">
                <div class="reward__description-info">
                    <p>
                        23 января закончилось голосование за самые популярные интерьеры<br>
                        и 3D-проекты 2016 года.
                    </p>
                    <p>2016</p>
                </div>
            </div>
        </fieldset>
    </div>
</div>