<div id="team" class="team">
    <div class="wrapper">
        <h2 class="title-line team__title">
            Наша команда
        </h2>
        <?= \frontend\widgets\MainTeam::widget() ?>
    </div>
</div>