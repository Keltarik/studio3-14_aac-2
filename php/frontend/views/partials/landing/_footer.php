<div class="footer wrapper">
    <span class="nav__logo">
        <img src="<?= Yii::getAlias('@web') ?>/img/logo.png" alt="">
        <span class="nav__logo-about">
           «Студия 3.14» — творческая команда дизайнеров, архитекторов, визуализаторов,<br> объединившаяся в 2013 году для создания «под ключ» интерьеров комфорт и бизнес-класса.
        </span>
    </span>
    <a  href="<?= Yii::getAlias('@web') ?>/pdf/policy.pdf" data-fancybox="" class="privacy-policy">
        Политика конфиденциальности
    </a>
</div>
<link href="/style/style.css" rel="stylesheet">
<!-- Google Analititcs -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-49553154-1', 'auto');
    ga('require', 'displayfeatures');
    ga('send', 'pageview');

    /* Accurate bounce rate by time */
    if (!document.referrer || document.referrer.split('/')[2].indexOf(location.hostname) != 0) {
        setTimeout(function() {
            ga('send', 'event', 'Новый посетитель', location.pathname);
        }, 15000);
    }
</script>

<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','//connect.facebook.net/en_US/fbevents.js');

    fbq('init', '1674242396123149');
    fbq('track', "PageView");
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=1674242396123149&ev=PageView&noscript=1"
    /></noscript>
<!-- End Facebook Pixel Code -->

<div style="visibility:hidden;position:absolute;bottom:20px;">
    <!-- begin of Top100 code -->

    <script id="top100Counter" type="text/javascript" src="http://counter.rambler.ru/top100.jcn?3009817"></script>
    <noscript>
        <a href="http://top100.rambler.ru/navi/3009817/">
            <img src="http://counter.rambler.ru/top100.cnt?3009817" alt="Rambler's Top100" border="0" />
        </a>

    </noscript>
    <!-- end of Top100 code -->
</div>

<script>
    (function(w, d, s, h, id) {
        w.roistatProjectId = id; w.roistatHost = h;
        var p = d.location.protocol == "https:" ? "https://" : "http://";
        var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
        var js = d.createElement(s); js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
    })(window, document, 'script', 'cloud.roistat.com', '3d47b23086c9e9c31c81955cbb22f9e1');
</script>