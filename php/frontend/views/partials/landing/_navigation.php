<?php

use yii\helpers\Url;

?>

<div class="nav">
    <div class="wrapper">
        <a href="<?= Url::to("@web/") ?>" class="nav__logo">
            <img src="<?= Yii::getAlias('@web') ?>/img/logo.png" alt="">
            <span class="nav__logo-description">
                От дизайн-проекта<br>
                до отделки и полного<br>
                оснащения
            </span>
        </a>
        <ul class="nav__list">
            <li><a href="#project">Портфолио</a></li>
            <li><a href="#price">Цены</a></li>
            <li><a href="#team">О нас</a></li>
            <li><a href="#contacts">Контакты</a></li>
            <li><a href="http://studio3-14.ru/partners">Партнеры</a></li>
            <li><a href="http://blog.studio3-14.ru/">Блог</a></li>
        </ul>
        <a href="javascript:;" class="nav__phone">
            <i class="icon"><svg><use xlink:href="#icon_telephone"></use></svg></i>
            <span class="nav__phone-number">
                8 499 398-17-33
            </span>
        </a>
    </div>
</div>