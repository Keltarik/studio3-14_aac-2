<div class="header">
    <div class="header__information">
        <div class="header__information-content">
            <h2 class="header__title">
                Красивые<br>
                и функциональные<br>
                интерьеры для квартир<br>
                и домов <span>по понятной цене</span>
            </h2>
            <div class="header__info-section">
                <div class="header__info">
                    <img src="<?= Yii::getAlias('@web') ?>/img/icons/header__icon.png" alt="">
                    <p>
                        Персональная<br>
                        проработка идеи
                    </p>
                </div>
                <div class="header__info">
                    <img src="<?= Yii::getAlias('@web') ?>/img/icons/header__icon.png" alt="">
                    <p>
                        11 готовых концепций<br>
                        с фиксированной ценой
                    </p>
                </div>
            </div>
            <div class="header__img">
                <img src="<?= Yii::getAlias('@web') ?>/img/icons/header__img-1.png" alt="">
                <img src="<?= Yii::getAlias('@web') ?>/img/icons/header__img-2.png" alt="">
            </div>
            <div class="header__send">
                <a href="javascript:;" class="header__btn btn open-modal">
                    <span>Посмотреть</span>
                    <i class="icon"><svg><use xlink:href="#icon_btn"></use></svg></i>
                </a>
                <p class="header__project-count">
                    113 проектов<br>
                    20 дизайнеров студии
                </p>
            </div>
        </div>
    </div>
    <div class="header__slider">
        <?= \frontend\widgets\MainSlider::widget() ?>
    </div>
</div>