<div id="contacts" class="map">
    <div id="map"></div>
    <div class="wrapper">
        <h2 class="map__title title-line">
            Контакты
        </h2>
        <div class="map__contact">
            <p class="map__contact-phone">
                <i class="icon"><svg><use xlink:href="#icon_telephone"></use></svg></i>
                <span class="nav__phone-number">
                    8 499 398-17-33
                </span>
            </p>
            <a href="javascript:;" class="btn map__contact-btn open-modal">
                <span>Заказать звонок</span>
                <i class="icon"><svg><use xlink:href="#icon_telephone"></use></svg></i>
            </a>
            <a href="mailto:info@studio3-14.ru" class="map__contact-email">
                <i class="icon"><svg><use xlink:href="#icon_email"></use></svg></i>
                <span>info@studio3-14.ru</span>
            </a>
        </div>
    </div>
</div>