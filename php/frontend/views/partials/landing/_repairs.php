<div class="repairs">
    <div class="wrapper">
        <h2 class="repairs__title title-line">
            Ремонт и отделка<br>
            помещений
        </h2>
        <div class="repairs__description">
            С нами вы получите ремонт, выполненный с профессиональной точностью<br>
            и в установленный срок. Мы воплотим любую вашу идею.
        </div>
        <ul class="repairs__list">
            <li>
                <i class="icon"><svg><use xlink:href="#icon_pencil"></use></svg></i>
                <p class="repairs__list-title">
                    Капитальный ремонт
                </p>
                <p class="repairs__list-description">
                    Работы по обновлению стен, потолка, пола,<br>
                    замена  электроточек в короткий срок
                </p>
            </li>
            <li>
                <i class="icon"><svg><use xlink:href="#icon_tassel"></use></svg></i>
                <p class="repairs__list-title">
                    Дизайнерский ремонт
                </p>
                <p class="repairs__list-description">
                    Удаление старой отделки вплоть до перекрытий,<br>
                    с полной заменой устаревших систем<br>
                    коммуникаций
                </p>
            </li>
            <li>
                <i class="icon"><svg><use xlink:href="#icon_palette"></use></svg></i>
                <p class="repairs__list-title">
                    Премиум ремонт
                </p>
                <p class="repairs__list-description">
                    Индивидуальный дизайн-проект с учетом<br>
                    ваших пожеланий. Строители профессионалы<br>
                    воплотят данный проект в жизнь.
                </p>
            </li>
        </ul>
        <div class="repairs__price">
            <p class="repairs__price-title">
                Цены<br>
                на ремонт
            </p>
            <p class="repairs__price-count">
                от <b>8 000</b> <span>f</span> /м <sub>2</sub>
            </p>
            <p class="repairs__price-description">
                В зависимости от<br>
                утвержденной сметы на<br>
                работы и использованных<br>
                чистовых материалов
            </p>
            <a href="javascript:;" class="btn repairs__price-btn open-modal">
                <span>Оставить заявку на консультацию</span>
                <i class="icon"><svg><use xlink:href="#icon_pen"></use></svg></i>
            </a>
        </div>
    </div>
</div>