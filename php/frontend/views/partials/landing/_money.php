<div class="money wrapper">
    <h2 class="money__title">
        Удобная и безопасная<br>
        оплата
    </h2>
    <div class="money__blocks">
        <div class="money__block">
            <div class="money__block-images">
                <img src="<?= Yii::getAlias('@web') ?>/img/money/1.png" alt="">
                <img src="<?= Yii::getAlias('@web') ?>/img/money/2.png" alt="">
            </div>
            <div class="money__block-info">
                <p class="money__block-info-title">
                    Оплачивайте как вам удобно
                </p>
                <p class="money__block-info-description">
                    Помимо наличного и безналичного расчета<br>
                    мы принимаем карты Visa и Mastercard.
                </p>
            </div>
        </div>
        <div class="money__block">
            <div class="money__block-images">
                <img src="<?= Yii::getAlias('@web') ?>/img/money/3.png" alt="">
                <img src="<?= Yii::getAlias('@web') ?>/img/money/4.png" alt="">
            </div>
            <div class="money__block-info">
                <p class="money__block-info-title">
                    Наши банки-партнеры
                </p>
                <p class="money__block-info-description">
                    Безопасность обработки интернет-<br>
                    платежей гарантирует ОАО «Альфа-Банк»
                </p>
            </div>
        </div>
        <div class="money__block">
            <div class="money__block-images">
                <img src="<?= Yii::getAlias('@web') ?>/img/money/5.png" alt="">
            </div>
            <div class="money__block-info">
                <p class="money__block-info-title">
                    Поэтапная оплата
                </p>
                <p class="money__block-info-description">
                    Работаем по поэтапной оплате,<br>
                    до полного результата
                </p>
            </div>
        </div>
    </div>
</div>