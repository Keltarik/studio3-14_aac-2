<div class="draft">
    <div class="wrapper">
        <h2 class="draft__title">
            У нас вы получите<br>
            проект с очень высокой<br>
            детализацией
        </h2>
        <p class="draft__subtitle">
            + все необходимые чертежи для строителей
        </p>
        <p class="draft__description">
            В состав нашего альбома дизайн-проекта входят все<br>
            необходимые разделы - от концепции и зонирования<br>
            помещений до ведомости комплектации и<br>
            3D-визуализации всех помещений.
        </p>
        <a href="javascript:;" class="btn draft__btn open-modal">
            <span>Заказать проект</span>
            <i class="icon"><svg><use xlink:href="#icon_draft"></use></svg></i>
        </a>
    </div>
</div>