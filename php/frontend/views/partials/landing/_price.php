<?php

/* @var $this \yii\web\View */
/* @var $callbackModel \frontend\models\form\AddReport */

use yii\helpers\Html;
use yii\helpers\Url;


?>

<div id="price" class="price">
    <div class="wrapper">
        <h2 class="price__title title-line">
            Стоимость<br>
            дизайн-проекта
        </h2>
        <table class="price__table">
            <tr>
                <th></th>
                <th class="price__table-all">Все включено</th>
                <th class="price__table-premium">Премиум</th>
                <th class="price__table-comfort">Комфорт</th>
            </tr>
            <tr>
                <td>До 80 кв.м.</td>
                <td>4 000<span>f</span></td>
                <td>2 200<span>f</span></td>
                <td>1 800<span>f</span></td>
            </tr>
            <tr>
                <td>От 81 до 150 кв.м.</td>
                <td>3 700<span>f</span></td>
                <td>2 090<span>f</span></td>
                <td>1 710<span>f</span></td>
            </tr>
            <tr>
                <td>От 151 до 250 кв.м.</td>
                <td>3 400<span>f</span></td>
                <td>1 970<span>f</span></td>
                <td>1 650<span>f</span></td>
            </tr>
            <tr>
                <td>От 251 до 400 кв.м.</td>
                <td>2 900<span>f</span></td>
                <td>1 700<span>f</span></td>
                <td>1 400<span>f</span></td>
            </tr>
            <!--<tr>
                <td>От 401 до 600 кв.м.</td>
                <td>3 500<span>f</span></td>
                <td>2 220<span>f</span></td>
                <td>1 800<span>f</span></td>
            </tr>
            <tr>
                <td>Более 601 кв.м.</td>
                <td>3 500<span>f</span></td>
                <td>2 200<span>f</span></td>
                <td>1 800<span>f</span></td>
            </tr>-->
        </table>
        <div class="price__questions">
            <h3 class="price__questions-title title-line">
                У вас есть вопросы?
            </h3>
            <p class="price__questions-description">
                Вы всегда можете связаться с нашим<br>
                дизайнером по телефону
            </p>
            <a href="javascript:;" class="nav__phone price__questions-phone">
                <i class="icon telephone"><svg><use xlink:href="#icon_telephone"></use></svg></i>
                <span>
                    8 499 398-17-33
                </span>
            </a>
        </div>
        <?= Html::beginForm(Url::to('@web/consult'), 'POST', ['id' => 'modal-callback-form']) ?>
            <div class="price__form-inputs">
                <label for="price_name">
                    <i class="icon"><svg><use xlink:href="#icon_person"></use></svg></i>
                    <?= Html::activeTextInput($callbackModel, 'user_name', ['id' => 'price_name', 'placeholder' => 'Ваше имя']) ?>
                </label>
                <label for="price_phone">
                    <i class="icon"><svg><use xlink:href="#icon_phone"></use></svg></i>
                    <?= Html::activeTextInput($callbackModel, 'user_phone', ['id' => 'price_phone', 'placeholder' => 'Телефон']) ?>
                </label>
                <label for="price_email">
                    <i class="icon"><svg><use xlink:href="#icon_email"></use></svg></i>
                    <?= Html::activeTextInput($callbackModel, 'user_email', ['id' => 'price_email', 'placeholder' => 'Электронная почта']) ?>
                </label>
            </div>
            <div class="price__form-buttons">
                <button class="order__form-submit btn">
                    <span>Оставить заявку</span>
                    <i class="icon"><svg><use xlink:href="#icon_pen"></use></svg></i>
                </button>
                <p class="order__form-person">
                    Нажимая на кнопку «Оставить заявку»,<br>
                    <span>Вы соглашаетесь на <a href="<?= Yii::getAlias('@web') ?>/pdf/policy.pdf" data-fancybox="">обработку персональных данных.</a></span>
                </p>
            </div>
        <?= Html::endForm() ?>
    </div>
</div>