<div class="ready wrapper">
    <div class="ready__info">
        <h2 class="ready__title title-line">
            8 готовых стилей, которые<br>
            нужно только адаптировать
        </h2>
        <p class="ready__subtitle">
            Для людей, которые не хотят активно<br>
            погружаться в создание пространства и готовы<br>
            довериться команде профессионалов
        </p>
        <p class="ready__description">
            Наш первый проект "<b>Студия 3.14</b>" входит в группу компаний "Пространство для развития". Мы хотим познакомить вас со вторым нашим проектом, который также входит в группу и называется он: "<b>Tim&Team</b>".<br><br>

            Данный проект предлагает вам сэкономить время и получить готовый интерьер по заранее разработанному стилю. Мы поможем адаптировать его под ваше пространство для жилья и вам останется лишь наслаждаться жизнью в нем.
        </p>
        <a href="http://timand.team" target="_blank"  class="ready__btn btn ">
            Посмотреть сайт с готовыми стилями
            <i class="icon"><svg><use xlink:href="#icon_btn"></use></svg></i>
        </a>
    </div>
    <div class="ready__slide">
        <div class="mcs-horizontal-example">
            <ul>
                <li>
                                    <a href="http://timand.team" target="_blank" class="ready__slide-link">
                        <img class="ready__slide-preview mCS_img_loaded" src="/img/ready/1_big.jpg" alt="">
                        <p class="ready__slide-title">
                            <span>Эко стиль</span><span> </span>
                        </p>
                        <p class="ready__slide-list">
                            <img src="/img/ready/1_small_1.jpg" alt="" class="mCS_img_loaded">
                            <img src="/img/ready/1_small_2.jpg" alt="" class="mCS_img_loaded">
                            <img src="/img/ready/1_small_3.jpg" alt="" class="mCS_img_loaded">
                        </p>
                    </a>
					
					                <a href="http://timand.team" target="_blank"  class="ready__slide-link">
                        <img class="ready__slide-preview mCS_img_loaded" src="/img/ready/2_big.jpg" alt="">
                        <p class="ready__slide-title">
                            <span>Современная классика</span><span> </span>
                        </p>
                        <p class="ready__slide-list">
                            <img src="/img/ready/2_small_1.jpg" alt="" class="mCS_img_loaded">
                            <img src="/img/ready/2_small_2.jpg" alt="" class="mCS_img_loaded">
                            <img src="/img/ready/2_small_3.jpg" alt="" class="mCS_img_loaded">
                        </p>
                    </a>
					
					
					                <a href="http://timand.team" target="_blank"  class="ready__slide-link">
                        <img class="ready__slide-preview mCS_img_loaded" src="/img/ready/3_big.jpg" alt="">
                        <p class="ready__slide-title">
                            <span>Скандинавия</span><span> </span>
                        </p>
                        <p class="ready__slide-list">
                            <img src="/img/ready/3_small_1.jpg" alt="" class="mCS_img_loaded">
                            <img src="/img/ready/3_small_2.jpg" alt="" class="mCS_img_loaded">
                            <img src="/img/ready/3_small_3.jpg" alt="" class="mCS_img_loaded">
                        </p>
                    </a>
					
					                <a href="http://timand.team" class="ready__slide-link">
                        <img class="ready__slide-preview mCS_img_loaded" src="/img/ready/4_big.jpg" alt="">
                        <p class="ready__slide-title">
                            <span>Светлый лофт</span><span> </span>
                        </p>
                        <p class="ready__slide-list">
                            <img src="/img/ready/4_small_1.jpg" alt="" class="mCS_img_loaded">
                            <img src="/img/ready/4_small_2.jpg" alt="" class="mCS_img_loaded">
                            <img src="/img/ready/4_small_3.jpg" alt="" class="mCS_img_loaded">
                        </p>
                    </a>
                </li>
                <li>
                       <a href="http://timand.team" target="_blank"  class="ready__slide-link">
                        <img class="ready__slide-preview mCS_img_loaded" src="/img/ready/5_big.jpg" alt="">
                        <p class="ready__slide-title">
                            <span>Темный лофт</span><span> </span>
                        </p>
                        <p class="ready__slide-list">
                            <img src="/img/ready/5_small_1.jpg" alt="" class="mCS_img_loaded">
                            <img src="/img/ready/5_small_2.jpg" alt="" class="mCS_img_loaded">
                            <img src="/img/ready/5_small_3.jpg" alt="" class="mCS_img_loaded">
                        </p>
                    </a>
					
					                <a href="http://timand.team" target="_blank"  class="ready__slide-link">
                        <img class="ready__slide-preview mCS_img_loaded" src="/img/ready/6_big.jpg" alt="">
                        <p class="ready__slide-title">
                            <span>Минимализм Black & White</span><span> </span>
                        </p>
                        <p class="ready__slide-list">
                            <img src="/img/ready/6_small_1.jpg" alt="" class="mCS_img_loaded">
                            <img src="/img/ready/6_small_2.jpg" alt="" class="mCS_img_loaded">
                            <img src="/img/ready/6_small_3.jpg" alt="" class="mCS_img_loaded">
                        </p>
                    </a>
					
					                <a href="http://timand.team" target="_blank"  class="ready__slide-link">
                        <img class="ready__slide-preview mCS_img_loaded" src="/img/ready/7_big.jpg" alt="">
                        <p class="ready__slide-title">
                            <span>Современный</span><span> </span>
                        </p>
                        <p class="ready__slide-list">
                            <img src="/img/ready/7_small_1.jpg" alt="" class="mCS_img_loaded">
                            <img src="/img/ready/7_small_2.jpg" alt="" class="mCS_img_loaded">
                            <img src="/img/ready/7_small_3.jpg" alt="" class="mCS_img_loaded">
                        </p>
                    </a>
					
					                <a href="http://timand.team" target="_blank"  class="ready__slide-link">
                        <img class="ready__slide-preview mCS_img_loaded" src="/img/ready/8_big.jpg" alt="">
                        <p class="ready__slide-title">
                            <span>Минимализм</span><span> </span>
                        </p>
                        <p class="ready__slide-list">
                            <img src="/img/ready/8_small_1.jpg" alt="" class="mCS_img_loaded">
                            <img src="/img/ready/8_small_2.jpg" alt="" class="mCS_img_loaded">
                            <img src="/img/ready/8_small_3.jpg" alt="" class="mCS_img_loaded">
                        </p>
                    </a>
                </li>
                
            </ul>
        </div>
    </div>
</div>
