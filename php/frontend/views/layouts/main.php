<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;

AppAsset::register($this);

$controller = Yii::$app->controller;
$default_controller = Yii::$app->defaultRoute;
$isHome = (($controller->id === $default_controller) && ($controller->action->id === $controller->defaultAction)) ? true : false;
$description = "«Студия 3.14» — стильный и практичный дизайн интерьера в Москве и Подмосковье";

?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=1320, initial-scale=0" name="viewport">
        <link rel="apple-touch-icon" sizes="57x57" href="<?= Yii::getAlias('@web') ?>/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?= Yii::getAlias('@web') ?>/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= Yii::getAlias('@web') ?>/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?= Yii::getAlias('@web') ?>/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= Yii::getAlias('@web') ?>/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?= Yii::getAlias('@web') ?>/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?= Yii::getAlias('@web') ?>/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?= Yii::getAlias('@web') ?>/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= Yii::getAlias('@web') ?>/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?= Yii::getAlias('@web') ?>/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= Yii::getAlias('@web') ?>/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?= Yii::getAlias('@web') ?>/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= Yii::getAlias('@web') ?>/favicon/favicon-16x16.png">
        <link rel="manifest" href="<?= Yii::getAlias('@web') ?>/favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?= Yii::getAlias('@web') ?>/favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <!-- Chrome, Firefox OS and Opera -->
        <meta name="theme-color" content="#000">
        <!-- Windows Phone -->
        <meta name="msapplication-navbutton-color" content="#000">
        <!-- iOS Safari -->
        <meta name="apple-mobile-web-app-status-bar-style" content="#000">
        <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
        <script>
            ymaps.ready(function () {
                var myMap = new ymaps.Map('map', {
                        center: [55.713, 37.582],
                        zoom: 16
                    }, {
                        searchControlProvider: 'yandex#search'
                    }),

                    // Создаём макет содержимого.
                    MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
                        '<div style="color: #FFFFFF; font-weight: bold; border-radius: 40px;">$[properties.iconContent]</div>'
                    ),

                    myPlacemarkWithContent = new ymaps.Placemark([55.7115, 37.5818], {
//                    hintContent: 'Собственный значок метки с контентом',
//                    balloonContent: '<img src="assets/img/bg/map_info.png">',
//                    iconContent: '12'
                    }, {
                        // Опции.
                        // Необходимо указать данный тип макета.
                        iconLayout: 'default#imageWithContent',
                        // Своё изображение иконки метки.
                        iconImageHref: '<?= Yii::getAlias('@web') ?>/img/map__pin.png',
                        // Размеры метки.
                        iconImageSize: [28, 28],
                        // Смещение левого верхнего угла иконки относительно
                        // её "ножки" (точки привязки).
                        iconImageOffset: [-28, -28],
                        // Смещение слоя с содержимым относительно слоя с картинкой.
                        iconContentOffset: [0, 0],
                        // Макет содержимого.
                        iconContentLayout: MyIconContentLayout
                    });

                myMap.geoObjects
                    .add(myPlacemarkWithContent);
                myMap.behaviors.disable('scrollZoom');
            });
        </script>
        <script data-skip-moving="true">
            (function(w,d,u,b){
                s=d.createElement('script');r=(Date.now()/1000|0);s.async=1;s.src=u+'?'+r;
                h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
            })(window,document,'https://cdn.bitrix24.ru/b2205599/crm/site_button/loader_2_d3lbtg.js');
        </script>

        <!--[if lt IE 9]>
        <script src="<?= Yii::getAlias('@web') ?>/js/libs/html5shiv.min.js"></script>
        <script src="<?= Yii::getAlias('@web') ?>/js/libs/respond.min.js"></script>
        <![endif]-->

        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?> 
    <style>
        .header,.nav{background-color:#f2f3f4}a,abbr,acronym,address,applet,article,aside,audio,b,big,blockquote,body,canvas,caption,center,cite,code,dd,del,details,dfn,div,dl,dt,em,embed,figcaption,figure,footer,form,h1,h3,h4,h5,h6,header,hgroup,html,i,iframe,img,ins,kbd,legend,li,mark,menu,nav,object,ol,output,p,pre,q,ruby,s,samp,section,small,span,strike,strong,sub,summary,sup,table,tbody,td,tfoot,th,thead,time,tr,tt,u,ul,var,video{font:inherit;font-size:100%;margin:0;padding:0;vertical-align:baseline;border:0}.nav__phone-number,h2{font-weight:700;color:#1d1d1d}body{line-height:1;min-width:1194px}.nav{padding-top:15px;padding-bottom:15px}.wrapper{width:1194px;margin:0 auto}.nav__list{flex-direction:row;min-width:602px;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-webkit-flex-direction:row;-moz-box-orient:horizontal;-moz-box-direction:normal;-ms-flex-direction:row;-webkit-box-pack:justify;-webkit-justify-content:space-between;-moz-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;margin-left:auto}.nav__list,.nav__phone{display:-webkit-box;display:-webkit-flex;display:-moz-box;display:-ms-flexbox;display:flex}.map__contact-phone,.nav__list li a,.nav__phone,.nav__phone-number{text-decoration:none}.nav__list li a{font-family:museo_sans_cyrl300;font-size:15px;text-transform:uppercase;color:#1d1d1d}.nav__phone-number{font-family:'Playfair Display',serif;font-size:20px;line-height:20px;padding-left:10px;letter-spacing:.04em}.map__contact-phone svg,.nav__phone svg{width:18px;height:18px;fill:#e92518}.nav__logo-about,.nav__logo-description{font-family:museo_sans_cyrl100_italic;font-size:11px;padding-left:14px;letter-spacing:.02em;color:#1d1d1d}.btn,.order__form-submit,h2{font-family:'Playfair Display',serif}ol,ul{list-style:none}.header{display:-webkit-box;display:-webkit-flex;display:-moz-box;display:-ms-flexbox;display:flex;min-height:608px}.header__information{width:50%;min-width:597px}.header__information-content{float:right;width:597px;padding-top:10px}h2{font-size:42px;line-height:50px;letter-spacing:.02em}.header__title span{font-style:italic;padding-left:80px;letter-spacing:.04em;color:#1d1d1d}.header__title span,.owl-dot.active:before,.owl-dots:after{font-family:'Playfair Display',serif;font-size:20px;font-weight:400}.header__info-section{display:inline-block;width:100%}.header__img,.header__info{display:-webkit-box;display:-webkit-flex;display:-moz-box;display:-ms-flexbox}.header__info{display:flex;float:left;margin-right:8px;padding-top:20px;-webkit-box-align:center;-webkit-align-items:center;-moz-box-align:center;-ms-flex-align:center;align-items:center}.header__img{display:flex;width:379px;margin:61px 0 0 40px;-webkit-box-align:center;-webkit-align-items:center;-moz-box-align:center;-ms-flex-align:center;align-items:center}.btn,.header__send,.order__form-submit{display:-webkit-box;display:-webkit-flex;display:-moz-box;display:-ms-flexbox}.header__slider{width:50%}.header__send{display:flex;margin-top:32px;-webkit-box-align:center;-webkit-align-items:center;-moz-box-align:center;-ms-flex-align:center;align-items:center}.btn i svg,.order__form-submit i svg{width:24px;height:24px;fill:#fff}.btn,.order__form-submit{font-size:15px;font-weight:700;display:flex;width:262px;height:54px;margin:24px auto 31px;padding:0!important;cursor:pointer;letter-spacing:.1em;text-transform:uppercase;color:#fff;border:0;background-color:#e92518;-webkit-box-align:center;-webkit-align-items:center;-moz-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-webkit-justify-content:center;-moz-box-pack:center;-ms-flex-pack:center;justify-content:center}.header__img img{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;height:130px;padding:19px 22px;border:1px solid #fff}.nav .wrapper,.nav__logo{display:-webkit-box;display:-webkit-flex;display:-moz-box;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-webkit-justify-content:space-between;-moz-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;-webkit-box-align:center;-webkit-align-items:center;-moz-box-align:center;-ms-flex-align:center;align-items:center}
    </style>
    </head>
    <body>
    <?php $this->beginBody() ?>

    <div hidden>
        <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
            <symbol viewBox="0 0 18 18" id="icon_telephone"><title>spb</title>
                <path d="M17.186,12.115l-1.951-1.395a1.879,1.879,0,0,0-2.429.2l-1.143,1.146A36.672,36.672,0,0,1,5.912,6.308L7.055,5.164a1.881,1.881,0,0,0,.2-2.43L5.863,0.778A1.885,1.885,0,0,0,3,.542l-1.8,1.8c-1.618,1.62-1.616,3.812,0,6.014a36.586,36.586,0,0,0,8.417,8.427c2.034,1.5,4.271,1.741,6.006,0l1.8-1.8A1.89,1.89,0,0,0,17.186,12.115Zm-0.756,1.875-1.8,1.8c-1.248,1.25-2.763.9-4.179-.142a35.31,35.31,0,0,1-8.119-8.13C1.288,6.1.941,4.589,2.189,3.339l1.8-1.8a0.479,0.479,0,0,1,.73.06L6.113,3.55a0.48,0.48,0,0,1-.051.619L4.54,5.693a0.7,0.7,0,0,0-.165.732c0.485,1.368,5.808,6.7,7.172,7.18a0.7,0.7,0,0,0,.732-0.166L13.8,11.916a0.477,0.477,0,0,1,.618-0.05l1.951,1.394A0.48,0.48,0,0,1,16.429,13.991Z"></path>
            </symbol>
            <symbol viewBox="0 0 17 20" id="icon_person">
                <path d="M8.44,10.774H8.572a3.82,3.82,0,0,0,2.92-1.263c1.595-1.8,1.329-4.879,1.3-5.172A4.224,4.224,0,0,0,10.784.582,4.606,4.606,0,0,0,8.556,0H8.485A4.6,4.6,0,0,0,6.258.565,4.229,4.229,0,0,0,4.224,4.338c-0.029.293-.294,3.375,1.3,5.172A3.8,3.8,0,0,0,8.44,10.774ZM5.33,4.442c0-.012,0-0.025,0-0.033C5.471,1.44,7.579,1.12,8.481,1.12h0.05c1.118,0.024,3.019.48,3.147,3.288a0.082,0.082,0,0,0,0,.033c0,0.029.294,2.845-1.023,4.328a2.714,2.714,0,0,1-2.133.886H8.485A2.7,2.7,0,0,1,6.357,8.77C5.044,7.3,5.326,4.467,5.33,4.442ZM17.017,15.885V15.873c0-.034,0-0.066,0-0.1-0.025-.82-0.079-2.739-1.876-3.35L15.1,12.407a11.929,11.929,0,0,1-3.437-1.567,0.559,0.559,0,0,0-.642.915A12.891,12.891,0,0,0,14.8,13.483c0.965,0.343,1.073,1.375,1.1,2.319a0.852,0.852,0,0,0,0,.1,7.515,7.515,0,0,1-.087,1.279,15.232,15.232,0,0,1-7.3,1.7,15.321,15.321,0,0,1-7.305-1.7A7.1,7.1,0,0,1,1.122,15.9c0-.033,0-0.066,0-0.1,0.029-.944.137-1.975,1.1-2.318a13.044,13.044,0,0,0,3.781-1.728,0.559,0.559,0,1,0-.642-0.915A11.808,11.808,0,0,1,1.93,12.4l-0.041.012c-1.8.616-1.851,2.535-1.876,3.35a0.85,0.85,0,0,1,0,.1v0.012a6.347,6.347,0,0,0,.211,1.876,0.533,0.533,0,0,0,.215.261A15.666,15.666,0,0,0,8.519,20,15.71,15.71,0,0,0,16.6,18.018a0.556,0.556,0,0,0,.215-0.261A6.663,6.663,0,0,0,17.017,15.885Z"></path>
            </symbol>
            <symbol viewBox="0 0 20 20" id="icon_phone">
                <path d="M14.179,19.99c0.09,0,.176.008,0.267,0.008a3.141,3.141,0,0,0,2.4-1.034l0.016-.017a9.748,9.748,0,0,1,.751-0.776c0.185-.176.374-0.357,0.55-0.544a1.9,1.9,0,0,0-.008-2.836l-2.34-2.338a1.918,1.918,0,0,0-1.392-.64,1.981,1.981,0,0,0-1.4.631l-1.384,1.383c-0.123-.069-0.25-0.131-0.373-0.192-0.156-.078-0.3-0.152-0.427-0.23a14.666,14.666,0,0,1-3.518-3.2A8.263,8.263,0,0,1,6.141,8.322c0.361-.329.7-0.669,1.022-1C7.282,7.2,7.4,7.078,7.524,6.956a1.859,1.859,0,0,0,0-2.84L6.363,2.954c-0.135-.135-0.267-0.271-0.4-0.4C5.706,2.291,5.439,2.015,5.172,1.769a1.944,1.944,0,0,0-1.383-.611,2.026,2.026,0,0,0-1.4.611L0.927,3.221a3.037,3.037,0,0,0-.9,1.933A7.251,7.251,0,0,0,.566,8.279a18.292,18.292,0,0,0,3.243,5.409,20.019,20.019,0,0,0,6.647,5.2A10.348,10.348,0,0,0,14.179,19.99ZM4.585,13.044A17.3,17.3,0,0,1,1.514,7.932a6.221,6.221,0,0,1-.484-2.7,2.008,2.008,0,0,1,.612-1.305L3.091,2.483a1.025,1.025,0,0,1,.7-0.324,0.985,0.985,0,0,1,.686.328c0.259,0.242.509,0.5,0.772,0.763,0.131,0.135.267,0.274,0.406,0.41L6.818,4.822a0.883,0.883,0,0,1,0,1.419C6.7,6.363,6.572,6.487,6.453,6.61c-0.361.365-.7,0.71-1.072,1.043l-0.021.02a0.783,0.783,0,0,0-.2.891c0,0.012.008,0.02,0.012,0.032a9.246,9.246,0,0,0,1.371,2.228,15.676,15.676,0,0,0,3.765,3.427c0.168,0.106.345,0.2,0.517,0.282,0.156,0.078.3,0.152,0.427,0.23,0.016,0.008.029,0.018,0.045,0.026a0.87,0.87,0,0,0,.39.1,0.823,0.823,0,0,0,.6-0.274l1.457-1.456a1,1,0,0,1,.694-0.336,0.979,0.979,0,0,1,.673.336L17.455,15.5a0.922,0.922,0,0,1-.012,1.433c-0.164.176-.337,0.34-0.517,0.516a10.6,10.6,0,0,0-.817.845,2.117,2.117,0,0,1-1.655.7c-0.066,0-.135,0-0.2-0.008a9.4,9.4,0,0,1-3.35-1A18.873,18.873,0,0,1,4.585,13.044ZM11,3.648a0.5,0.5,0,1,0-.168.99,5.548,5.548,0,0,1,4.52,4.516,0.5,0.5,0,0,0,.5.42,0.552,0.552,0,0,0,.086-0.008,0.5,0.5,0,0,0,.411-0.579A6.545,6.545,0,0,0,11,3.648ZM19.5,9.4a0.667,0.667,0,0,0,.086-0.008A0.5,0.5,0,0,0,20,8.813,10.825,10.825,0,0,0,11.17-.008,0.5,0.5,0,0,0,11,.982a9.825,9.825,0,0,1,8.006,8A0.5,0.5,0,0,0,19.5,9.4Z"></path>
            </symbol>
            <symbol viewBox="0 0 24 18" id="icon_email">
                <path d="M21.1,0.027H2.964A2.917,2.917,0,0,0,.03,2.92V15.136a2.916,2.916,0,0,0,2.934,2.891H21.091a2.916,2.916,0,0,0,2.934-2.891V2.924A2.913,2.913,0,0,0,21.1.027Zm1.594,15.109a1.584,1.584,0,0,1-1.594,1.57H2.964a1.584,1.584,0,0,1-1.594-1.57V2.924A1.585,1.585,0,0,1,2.964,1.353H21.091a1.585,1.585,0,0,1,1.594,1.571V15.136h0Zm-7.527-6.26,5.868-5.188a0.657,0.657,0,0,0,.05-0.935A0.68,0.68,0,0,0,20.133,2.7L12.04,9.865l-1.579-1.39a0.027,0.027,0,0,1-.01-0.015,1.029,1.029,0,0,0-.109-0.093L3.917,2.7a0.677,0.677,0,0,0-.948.054,0.653,0.653,0,0,0,.055.935L8.962,8.92,3.049,14.376a0.657,0.657,0,0,0-.03.935,0.688,0.688,0,0,0,.492.211,0.676,0.676,0,0,0,.457-0.176l6-5.536L11.6,11.244a0.676,0.676,0,0,0,.894,0l1.673-1.478,5.968,5.588a0.675,0.675,0,0,0,.948-0.025,0.655,0.655,0,0,0-.025-0.933Z"></path>
            </symbol>
            <symbol viewBox="0 0 24 24" id="icon_pen">
                <path d="M15.093,0.283L0.4,14.961a1.016,1.016,0,0,0-.285.648L0,23.082a0.881,0.881,0,0,0,.285.663A0.946,0.946,0,0,0,.949,24H0.965l7.262-.064a0.919,0.919,0,0,0,.664-0.253l14.824-14.8a0.962,0.962,0,0,0,0-1.342L16.437,0.267A0.981,0.981,0,0,0,15.093.283ZM7.815,22.07l-5.9.08,0.095-6.131,9.809-9.8,5.933,5.926ZM19.079,10.822L13.163,4.9l2.61-2.608L21.69,8.215Z"></path>
            </symbol>
            <symbol viewBox="0 0 24 24" id="icon_draft">
                <path class="cls-1" d="M20.484,2.81a0.7,0.7,0,1,1-.7.7A0.7,0.7,0,0,1,20.484,2.81Zm0-2.813a3.518,3.518,0,0,0-3.476,4.046L8.874,12.1,1.2,4.423a0.7,0.7,0,0,0-1.2.5V23.294a0.7,0.7,0,0,0,.7.7H19.078a0.7,0.7,0,0,0,.7-0.611l0.041,0.126a0.7,0.7,0,0,0,1.337,0L22.559,19.2a0.712,0.712,0,0,0,.035-0.218V6.324A3.515,3.515,0,0,0,20.484,0ZM17.565,5.47a3.55,3.55,0,0,0,.81.854V6.647l-7.512,7.439-0.994-.995Zm-8.7,8.611,0.994,0.993-0.112.112-1.92.9L8.752,14.2ZM5.828,18.07a0.7,0.7,0,0,0,.8.142l1.629-.767,2.24,2.241H4.219V13.407l2.257,2.257-0.78,1.6A0.7,0.7,0,0,0,5.828,18.07ZM1.406,22.592V6.617l6.469,6.47a1.38,1.38,0,0,0-.336.388l-0.414.851L4.013,11.213a0.7,0.7,0,0,0-1.2.5v8.679a0.7,0.7,0,0,0,.7.7h8.679a0.7,0.7,0,0,0,.5-1.2L9.608,16.808l0.852-.4a1.412,1.412,0,0,0,.4-0.338l6.522,6.523H1.406Zm18.169,0.2L11.858,15.08l6.517-6.454V18.983a0.712,0.712,0,0,0,.035.218l1.177,3.609Zm1.612-3.927-0.7,2.157-0.7-2.157V18.279h1.406V18.87Zm0-2H19.781V7.234L20.02,7a3.473,3.473,0,0,0,1.168-.039v9.914Zm-0.7-11.25a2.109,2.109,0,1,1,2.109-2.108A2.112,2.112,0,0,1,20.484,5.623Z"></path>
            </symbol>
            <symbol viewBox="0 0 61 61" id="icon_pencil">
                <path d="M46.968,47.789a3.87,3.87,0,0,0,0,5.6,3.863,3.863,0,0,0,5.6,0A3.961,3.961,0,1,0,46.968,47.789Zm4.2,4.3a1.981,1.981,0,1,1-2.8-2.8,1.936,1.936,0,0,1,2.8,0A1.935,1.935,0,0,1,51.168,52.092Zm8.2-3.1-18-18.011,7.1-7.1h0l4.2-4.2h0l6.1-6.1a4.411,4.411,0,0,0,0-6.2l-5.1-5.1a4.347,4.347,0,0,0-6.2,0l-6.1,6.1-4.2,4.2-7.1,7.1-17.8-17.81a3.086,3.086,0,0,0-4.2,0l-7.1,7.105a2.9,2.9,0,0,0,0,4.2l2.1,2.1h0l5.7,5.7h0l5.6,5.6h0l4.4,4.4L2.713,47.043a1.012,1.012,0,0,0-.547.746L0.066,59.8a1.223,1.223,0,0,0,.3.9,0.91,0.91,0,0,0,.7.3h0.2L12.8,58.864a0.978,0.978,0,0,0,.263.032,0.777,0.777,0,0,0,.7-0.3l16.3-16.309,1.3,1.3h0l2.8,2.8h0L48.068,60.3a2.337,2.337,0,0,0,1.7.7,1.985,1.985,0,0,0,1.6-.7l8-8A2.355,2.355,0,0,0,59.369,48.991Zm-46.3,7.4-3.5-3.5,33.25-33.171,3.451,3.452Zm35.8-52.73a2.478,2.478,0,0,1,3.4,0l5.1,5.1a2.48,2.48,0,0,1,0,3.4l-5.4,5.4-8.5-8.5Zm-6.8,6.8,4.25,4.253,4.25,4.252-2.8,2.8-8.5-8.506ZM19.317,28.73l2.15-2.151a0.991,0.991,0,0,0-1.4-1.4l-2.15,2.151-1.45-1.45,3.5-3.5a0.99,0.99,0,0,0-1.4-1.4l-3.5,3.5-1.4-1.4,2.1-2.1a0.991,0.991,0,1,0-1.4-1.4l-2.1,2.1-1.4-1.4,3.5-3.5a0.991,0.991,0,1,0-1.4-1.4l-3.5,3.5-1.5-1.5,2.1-2.1a0.99,0.99,0,0,0-1.4-1.4l-2.1,2.1-1.4-1.4,3.5-3.5a0.991,0.991,0,1,0-1.4-1.4l-3.5,3.5-1.4-1.4a0.967,0.967,0,0,1,0-1.4l7.1-7.1a0.908,0.908,0,0,1,.7-0.3,1.081,1.081,0,0,1,.7.3l17.8,17.81-8.5,8.505ZM37.868,14.771l3.5,3.5L8.117,51.443,4.666,47.991Zm-34,35.221,3.281,3.284a1.061,1.061,0,0,0,.219.319l3.615,3.616L2.366,58.7Zm54.1,0.9-8,8a0.453,0.453,0,0,1-.5,0L39.118,48.54l3.55-3.552a0.99,0.99,0,1,0-1.4-1.4l-3.55,3.551-1.45-1.45,2.1-2.1a0.991,0.991,0,0,0-1.4-1.4l-2.1,2.1-1.4-1.4,3.5-3.5a0.991,0.991,0,1,0-1.4-1.4l-3.5,3.5-0.6-.6,8.5-8.5,18,18.011A0.453,0.453,0,0,1,57.969,50.891Z"></path>
            </symbol>
            <symbol viewBox="0 0 60 60" id="icon_tassel">
                <path d="M30,54a4,4,0,1,0-4-4A4.012,4.012,0,0,0,30,54Zm0-6a2,2,0,1,1-2,2A2.006,2.006,0,0,1,30,48ZM59.717,1.7a0.912,0.912,0,0,0,.2-1.1,0.876,0.876,0,0,0-.9-0.6h-0.8a11.354,11.354,0,0,0-9.705,5.4c-0.1.2-.2,0.3-0.3,0.5a10.612,10.612,0,0,0-2.9-4.8,4.823,4.823,0,0,0-3-1.1H2.484a2.439,2.439,0,0,0-2.1,1.2,2.676,2.676,0,0,0-.1,2.6C1.4,5.75,1.909,8.736,1.976,13a0.944,0.944,0,0,0-.993,1v9a0.945,0.945,0,0,0,1,1v7a4.953,4.953,0,0,0,5,5l10.806,0.1a4.01,4.01,0,0,1,2.9,1.2,3.913,3.913,0,0,1,1.1,2.9L21.1,56.9a2.652,2.652,0,0,0,.8,2.2,3.1,3.1,0,0,0,2.2.9H35.9a2.7,2.7,0,0,0,2.2-1,3.5,3.5,0,0,0,.8-2.2L38.2,40.1a3.56,3.56,0,0,1,1.1-2.9,4.007,4.007,0,0,1,2.9-1.2H53.013a4.953,4.953,0,0,0,5-5V24a0.945,0.945,0,0,0,1-1V14a0.945,0.945,0,0,0-1-1H57.558C56.233,10.016,57.26,4.342,59.717,1.7ZM57.015,22H2.985V15H57.015v7Zm-1,9a2.947,2.947,0,0,1-3,3l-10.806.1a5.819,5.819,0,0,0-4.3,1.8,5.719,5.719,0,0,0-1.7,4.4L36.9,57a1.077,1.077,0,0,1-.3.7,0.909,0.909,0,0,1-.7.3H24.1a1.026,1.026,0,0,1-1-1l0.7-16.8a6.553,6.553,0,0,0-1.7-4.4,6.227,6.227,0,0,0-4.3-1.8H6.987a2.937,2.937,0,0,1-2.84-2h2.84a1,1,0,0,0,1-1,0.945,0.945,0,0,0-1-1h-3V28h5a1,1,0,1,0,0-2h-5V24H56.015v7h0Zm-7.7-18H3.985c-0.1-4.6-.7-7.9-1.9-10.1a1.144,1.144,0,0,1,0-.6,0.447,0.447,0,0,1,.4-0.3H42.307a3.228,3.228,0,0,1,1.8.6q2.1,1.8,3,6.6a0.962,0.962,0,0,0,.9.8,0.808,0.808,0,0,0,.9-0.7,9.927,9.927,0,0,1,1.2-2.9,9.224,9.224,0,0,1,6.8-4.3c-1.8,3.1-2.5,7.8-1.5,10.9h-7.1Z"></path>
            </symbol>
            <symbol viewBox="0 0 60 60" id="icon_palette">
                <path d="M57,42H43.241L55,35.4a3.784,3.784,0,0,0,1.8-2.3,3.667,3.667,0,0,0-.3-2.9L51.3,21a4.141,4.141,0,0,0-2.2-1.7,3.2,3.2,0,0,0-2.7.3L35.94,25.462,42.4,15.4a3.859,3.859,0,0,0-1.2-5.3L32.8,4.7a4.035,4.035,0,0,0-5.6,1.2L20,17.064V3a2.834,2.834,0,0,0-3-3H5A2.834,2.834,0,0,0,2,3V42.159A2.936,2.936,0,0,0,0,45V57a2.946,2.946,0,0,0,3,3H57a2.946,2.946,0,0,0,3-3V45A2.945,2.945,0,0,0,57,42ZM47.3,21.2a1.683,1.683,0,0,1,1.2-.1,1.266,1.266,0,0,1,1,.7L54.7,31a1.48,1.48,0,0,1,.2,1.3,1.55,1.55,0,0,1-.8,1.1L38.8,42H10.5l5.367-3.035L33.4,29.1a0.476,0.476,0,0,0,.347-0.239ZM28.9,7a2.175,2.175,0,0,1,2.9-.6l8.4,5.4a1.808,1.808,0,0,1,.6,2.5L32.3,27.5,6.4,42ZM5,2H17a0.859,0.859,0,0,1,1,1V20.166L4,41.877V3A0.859,0.859,0,0,1,5,2ZM58,57a0.945,0.945,0,0,1-1,1H3a0.945,0.945,0,0,1-1-1V45a0.945,0.945,0,0,1,1-1H39.1a0.6,0.6,0,0,1,.4.1l0.178-.1H57a1,1,0,0,1,1,1V57h0ZM26,46H18a0.944,0.944,0,0,0-1,1v8a0.945,0.945,0,0,0,1,1h8a0.945,0.945,0,0,0,1-1V47A0.944,0.944,0,0,0,26,46Zm-1,8H19V48h6v6Zm14-8H31a0.944,0.944,0,0,0-1,1v8a0.945,0.945,0,0,0,1,1h8a0.945,0.945,0,0,0,1-1V47A0.944,0.944,0,0,0,39,46Zm-1,8H32V48h6v6Zm14-8H44a0.944,0.944,0,0,0-1,1v8a0.945,0.945,0,0,0,1,1h8a0.945,0.945,0,0,0,1-1V47A0.944,0.944,0,0,0,52,46Zm-1,8H45V48h6v6ZM9,46A5,5,0,1,0,9,56,5,5,0,1,0,9,46Zm0,8a3,3,0,1,1,0-6A3,3,0,1,1,9,54Z"></path>
            </symbol>
            <symbol viewBox="0 0 22 24" id="icon_gift">
                <path d="M17.834,4.425a2.929,2.929,0,0,0,.372-1.43,3.031,3.031,0,0,0-3.059-3,4.8,4.8,0,0,0-3.5,1.731A6.156,6.156,0,0,0,11,2.6a6.148,6.148,0,0,0-.644-0.87A4.8,4.8,0,0,0,6.853,0a3.031,3.031,0,0,0-3.059,3,2.929,2.929,0,0,0,.372,1.43H0v8.714H1.435V24H20.565V13.139H22V4.425H17.834ZM6.853,1.563c1.45,0,2.829,1.453,3.231,2.862H6.853a1.447,1.447,0,0,1-1.461-1.43A1.448,1.448,0,0,1,6.853,1.563ZM10.2,22.433H3.034V13.139H10.2v9.294h0Zm0-10.859H1.6V5.99h8.6v5.584Zm4.946-10.01a1.448,1.448,0,0,1,1.461,1.431,1.447,1.447,0,0,1-1.461,1.43H11.916C12.317,3.017,13.7,1.563,15.146,1.563Zm3.82,20.869H11.8V13.139h7.167v9.294h0ZM20.4,11.574H11.8V5.99h8.6v5.584Z"></path>
            </symbol>
            <symbol viewBox="0 0 24 24" id="icon_btn">
                <path d="M12.724,0.424a0.815,0.815,0,0,0-1.41,0L7.828,6.476a0.813,0.813,0,0,0,.7,1.22h6.975a0.813,0.813,0,0,0,.679-1.261ZM9.94,6.07L12.02,2.462,14.1,6.07H9.94Zm5.566,10.268H8.533a0.814,0.814,0,0,0-.7,1.221l3.487,6.051a0.813,0.813,0,0,0,1.41,0l3.487-6.051A0.814,0.814,0,0,0,15.507,16.337ZM12.02,21.574L9.94,17.966H14.1ZM23.611,11.312L17.559,7.825a0.814,0.814,0,0,0-1.219.706v6.974a0.812,0.812,0,0,0,1.219.7l6.052-3.487A0.814,0.814,0,0,0,23.611,11.312ZM17.967,14.1V9.938l3.609,2.08ZM7.293,7.825a0.819,0.819,0,0,0-.813,0L0.428,11.313a0.813,0.813,0,0,0,0,1.409L6.48,16.209a0.808,0.808,0,0,0,.406.11A0.814,0.814,0,0,0,7.7,15.505V8.53A0.815,0.815,0,0,0,7.293,7.825ZM6.072,14.1l-3.609-2.08L6.072,9.938V14.1Z"></path>
            </symbol>
            <symbol viewBox="0 0 23 24" id="icon_search">
                <path d="M22.525,21.858l-5.639-5.941A9.711,9.711,0,0,0,9.564,0,9.64,9.64,0,0,0,0,9.689a9.639,9.639,0,0,0,9.566,9.689,9.386,9.386,0,0,0,5.481-1.753l5.682,5.985a1.235,1.235,0,0,0,1.764.035A1.277,1.277,0,0,0,22.525,21.858ZM9.564,2.526a7.162,7.162,0,0,1,0,14.324A7.162,7.162,0,0,1,9.564,2.526Z"></path>
            </symbol>
            <symbol viewBox="0 0 14 10" id="icon_left">
                <path d="M4.561,0.155a0.489,0.489,0,0,1,.706,0,0.511,0.511,0,0,1,0,.709L1.689,4.5H13.495a0.506,0.506,0,0,1,0,1.012H1.689L5.267,9.145a0.519,0.519,0,0,1,0,.716,0.486,0.486,0,0,1-.706,0l-4.425-4.5a0.5,0.5,0,0,1,0-.71Z"></path>
            </symbol>
            <symbol viewBox="0 0 14 10" id="icon_right">
                <path d="M9.439,0.155a0.489,0.489,0,0,0-.706,0,0.511,0.511,0,0,0,0,.709L12.311,4.5H0.506a0.506,0.506,0,0,0,0,1.012H12.311L8.733,9.145a0.519,0.519,0,0,0,0,.716,0.486,0.486,0,0,0,.706,0l4.424-4.5a0.5,0.5,0,0,0,0-.71Z"></path>
            </symbol>
            <symbol viewBox="0 0 17 18" id="icon_time">
                <path d="M16.6,11.044a7.657,7.657,0,0,1-15.245.014H0.6a0.6,0.6,0,0,1-.515-0.9L1.768,7.215a0.593,0.593,0,0,1,1.031,0L4.483,10.16a0.6,0.6,0,0,1-.515.9H3.312A5.726,5.726,0,1,0,8.973,4.491a5.658,5.658,0,0,0-3.154.955,1,1,0,0,1-1.355-.182A0.986,0.986,0,0,1,4.688,3.85,7.567,7.567,0,0,1,8.023,2.589V1.472H7.243A0.488,0.488,0,0,1,6.756.983V0.488A0.488,0.488,0,0,1,7.243,0h3.62a0.488,0.488,0,0,1,.486.49V0.983a0.488,0.488,0,0,1-.486.489H10V2.6A7.724,7.724,0,0,1,16.6,11.044ZM16.849,3.62L15.383,2.254a0.483,0.483,0,0,0-.687.027L14.308,2.7a0.493,0.493,0,0,0,.027.692L15.8,4.764a0.482,0.482,0,0,0,.687-0.027l0.388-.423A0.5,0.5,0,0,0,16.849,3.62ZM8.95,5.5v4.8h4.708A4.639,4.639,0,0,0,8.95,5.5Z"></path>
            </symbol>
        </svg>
    </div>

    <?= $content ?>

    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>