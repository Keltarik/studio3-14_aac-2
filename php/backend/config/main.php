<?php
$params = array_merge(
	require(__DIR__ . '/../../common/config/params.php'),
	require(__DIR__ . '/params.php')
);

$config = [
	'id' => 'admin',
	'basePath' => dirname(__DIR__),
	'bootstrap' => ['log'],

	'language' => 'ru-RU',
	'sourceLanguage' => 'ru-RU',

	'defaultRoute' => 'admin',

	'controllerNamespace' => 'backend\controllers',
	'components' => [

		'user' => [
			'identityClass' => 'backend\models\User',
			'enableAutoLogin' => true,
			'enableSession' => true,
		],
		'assetManager' => [
			'basePath' => '@webroot/assets',

			/*'bundles' => [ // будем брать Jquery из bower
				'yii\web\JqueryAsset' => [
					'baseUrl' => '@web',
					'sourcePath' => null,   // do not publish the bundle
					'js' => [
						'bower_components/jquery/dist/jquery.min.js',
					]
				],
			],*/
		],
		'request' => [
			// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
			'cookieValidationKey' => 'Sma1VeQfRTlX_tJ8zApF7p3NTybbyTDP',
		],
		'urlManager' => array(
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'rules' => [
				'<alias:index|projects|team|partners|report|login|logout|tags|slider>' => 'admin/<alias>'
			]
		)
	],
	'params' => $params,
];
return $config;