<?php
/**
 * Created by PhpStorm.
 * User: zachot
 * Date: 02.08.15
 * Time: 2:36
 */

namespace backend\models\edit;


use backend\models\type\Project;
use common\models\type\Tag;
use common\models\type\TagLink;
use yii\base\Model;

class EditProject extends Model {

	public $title;
	public $text;
	public $slug;
	public $titlepage;

	public $team = [];
	public $role = [];
	public $roles = [];

	public $room_ids = [];
    public $partner_ids = [];
    public $tags_arr = [];
    public $tags_str = '';
	public $review_ids = [];

	public $address;

	public $social_image;
	public $social_title;
	public $social_description;


	public $thumbnail;
	public $thumbnailback;

	/**
	 * @var Project
	 */
	private $_project;

	/**
	 * @return array
	 */
	public function rules() {
		return [
			[['title', 'slug', 'text', 'titlepage', 'thumbnail', 'address', 'thumbnailback'], 'required'],
			[['title', 'slug', 'text', 'titlepage', 'address', 'tags_str'], 'string'],
			[['social_title', 'social_description'], 'string'],
			[['social_image', 'thumbnail', 'thumbnailback'], 'number'],
			[['role', 'team', 'room_ids', 'partner_ids', 'review_ids', 'tags_arr'], 'checkIsArray'],
		];
	}

	public function checkIsArray() {
		if (!is_array($this->role)) {
			$this->addError('arr', 'arr is not array!');
		}
	}

	/**
	 * @param number $id
	 */
	public function loadProject($id) {
		/**
		 * @var $project Project
		 */
		$this->_project = $project = Project::findOne(["id" => $id]);
		if ($project) {
			$this->title = $project->getAttribute('title');
			$this->text = $project->getAttribute('text');
			$this->slug = $project->getAttribute('slug');
			$this->titlepage = $project->getAttribute('title-page');
			$this->address = $project->getAttribute('address');

			$team = $project->getAttribute('team');
			if ($team) {
				$this->roles = unserialize($team);
			}
			foreach ($this->roles as $index => $r) {
				$this->team[] = $r['id'];
				$this->role[] = $r['role'];
			}

			$this->_project->setAttribute('team', serialize($this->roles));
			$this->room_ids = explode(',', $project->getAttribute('room-ids'));
			$this->review_ids = explode(',', $project->getAttribute('review-ids'));
            $this->partner_ids = explode(',', $project->getAttribute('partner-ids'));
            $this->tags_arr = explode(',', $project->getAttribute('tags'));
            $this->tags_str = $project->getAttribute('tags');

			$this->thumbnail = $project->getAttribute('thumbnail');
			$this->thumbnailback = $project->getAttribute('thumbnail-back');


			$social = unserialize($project->getAttribute('social'));
			if (isset($social['title']))
				$this->social_title = $social['title'];
			if (isset($social['image']))
				$this->social_image = $social['image'];
			if (isset($social['description']))
				$this->social_description = $social['description'];


		}
	}

	public function save() {
		if (!$this->_project) {
			$this->addError('empty', 'true');
			$this->_project = new Project();
		}

		$this->_project->setAttribute('title', $this->title);
		$this->_project->setAttribute('text', $this->text);
		$this->_project->setAttribute('slug', $this->slug);
		$this->_project->setAttribute('title-page', $this->titlepage);
		$this->_project->setAttribute('address', $this->address);


		$this->roles = [];
		foreach ($this->team as $index => $t) {
			$this->roles[] = [
				"id" => $t,
				"role" => $this->role[$index]
			];
		}

		$this->_project->setAttribute('team', serialize($this->roles));

		$this->_project->setAttribute('thumbnail', $this->thumbnail);
		$this->_project->setAttribute('thumbnail-back', $this->thumbnailback);

		$this->_project->setAttribute('room-ids', implode(',', $this->room_ids) );
		$this->_project->setAttribute('review-ids', implode(',', $this->review_ids));
		$this->_project->setAttribute('team-ids', implode(',', $this->team));
        $this->_project->setAttribute('partner-ids', implode(',', $this->partner_ids));
        $this->tags_arr = explode(',', $this->tags_str);
        $this->_project->setAttribute('tags', implode(',', $this->tags_arr));
        // обрабатываем теги
        $tag_link_temp = TagLink::find()->where(['project_id' => $this->_project->getAttribute('id')])
            ->all();
        foreach ($tag_link_temp as $temp){
            $temp->delete();
        }

        foreach ($this->tags_arr as $tag) {
            if (trim($tag) != ''){
                if (!($tag_object = Tag::findOne(['name' => trim($tag)]))) {
                    $tag_instance = new Tag();
                    $tag_instance->name = trim($tag);
                    $tag_instance->save();
                    $tag_link = new TagLink();
                    $tag_link->tag_id = $tag_instance->id;
                    $tag_link->project_id = $this->_project->getAttribute('id');
                    $tag_link->save();
                } else {
                    $tag_link = new TagLink();
                    $tag_link->tag_id = $tag_object->id;
                    $tag_link->project_id = $this->_project->getAttribute('id');
                    $tag_link->save();
                }
            }
        }

		//SOCIAL

		$social = [];
		if ($this->social_title)
			$social['title'] = $this->social_title;
		if ($this->social_image)
			$social['image'] = $this->social_image;
		if ($this->social_description)
			$social['description'] = $this->social_description;

		$this->_project->setAttribute('social', serialize($social));


		$this->_project->save();

		$this->addErrors($this->_project->errors);

		return !$this->hasErrors();
	}

	/**
	 * @return mixed
	 */
	public function getId() {
		if ($this->_project)
			return $this->_project->getAttribute('id');
		return 0;
	}

	public function go() {
		return
			$this->load(\Yii::$app->request->post())
			&& $this->validate()
			&& $this->save();
	}
}