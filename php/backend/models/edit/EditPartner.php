<?php
/**
 * Created by IntelliJ IDEA.
 * User: dick
 * Date: 10/26/15
 * Time: 1:42 AM
 */

namespace backend\models\edit;


use backend\models\type\Partner;
use yii\base\Model;

class EditPartner extends Model {

    public $name;
    public $logo;
    public $about;

    public $visible;
    /**
     * @var Partner
     */
    private $_partner;

    public function rules() {
        return [
            [['name', 'logo', 'about'], 'required'],
            [['name', 'about'], 'string'],
            [['logo', 'visible'], 'number'],
        ];
    }


    /**
     * @param number $id
     */
    public function loadPartner($id) {
        /**
         * @var $partner Partner
         */
        $this->_partner = $partner = Partner::findOne(["id" => $id]);
        if ($partner) {
            $this->name = $partner->getAttribute('name');
            $this->logo = $partner->getAttribute('logo');
            $this->about = $partner->getAttribute('about');
            $this->visible = $partner->getAttribute('visible');
        }

    }

    /**
     * @return bool
     */
    public function save() {

        if (!$this->_partner) {
            $this->addError('empty', 'true');
            $this->_partner = new Partner();
        }

        $this->_partner->setAttribute('name', $this->name);
        $this->_partner->setAttribute('logo', $this->logo);
        $this->_partner->setAttribute('about', $this->about);
        $this->_partner->setAttribute('visible', $this->visible);

        $this->_partner->save();

        $this->addErrors($this->_partner->errors);

        return !$this->hasErrors();
    }

    /**
     * @return mixed
     */
    public function getId() {
        if ($this->_partner)
            return $this->_partner->getAttribute('id');
        return 0;
    }

    /**
     * @return bool
     */
    public function go() {
        return
            $this->load(\Yii::$app->request->post())
            && $this->validate()
            && $this->save();
    }
}