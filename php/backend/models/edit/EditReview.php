<?php
/**
 * Created by PhpStorm.
 * User: zachot
 * Date: 05.08.15
 * Time: 5:03
 */

namespace backend\models\edit;


use backend\models\type\Review;

use yii\base\Model;

class EditReview extends Model {
	public $author_name;
	public $author_profession;
	public $author_photo;

	public $text;
	public $main_title;
	public $main_text;
	public $video;

	/**
	 * @var Review
	 */
	private $_review;

	public $id;


	public function rules() {
		return [
			[['author_name', 'author_photo', 'text'], 'required'],
			[['author_name', 'author_profession', 'text', 'video', 'main_title', 'main_text'], 'string'],
			[['author_photo'], 'number']
		];
	}

	public function loadReview($id) {
		$this->id = $id;
		/**
		 * @var $room Review
		 */
		$this->_review = $room = Review::findOne(["id" => $id]);
		if ($room) {

			$this->text = $room->getAttribute('text');
			$this->main_title = $room->getAttribute('main_title');
			$this->main_text = $room->getAttribute('main_text');

			$this->video = $room->getAttribute('video');
			$this->author_name = $room->getAttribute('author_name');
			$this->author_photo = $room->getAttribute('author_photo');
			$this->author_profession = $room->getAttribute('author_profession');
		}
	}

	public function save() {
		if (!$this->_review) {
			$this->addError('empty', 'true');
			$this->_review = new Review();
		}

		$this->_review->setAttribute('text', $this->text);
		$this->_review->setAttribute('main_title', $this->main_title);
		$this->_review->setAttribute('main_text', $this->main_text);
		$this->_review->setAttribute('video', $this->video);


		$this->_review->setAttribute('author_name', $this->author_name);
		$this->_review->setAttribute('author_photo', $this->author_photo);
		$this->_review->setAttribute('author_profession', $this->author_profession);

		$this->_review->save();

		$this->id = $this->_review->getAttribute('id');

		$this->addErrors($this->_review->errors);

		return !$this->hasErrors();
	}

	/**
	 * @return mixed
	 */
	public function getId() {
		if ($this->_review)
			return $this->_review->getAttribute('id');
		return 0;
	}

	public function go() {
		return
			$this->load(\Yii::$app->request->post())
			&& $this->validate()
			&& $this->save();
	}
}