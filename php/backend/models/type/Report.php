<?php
/**
 * Created by PhpStorm.
 * User: zachot
 * Date: 05.08.15
 * Time: 17:49
 */

namespace backend\models\type;



class Report extends \common\models\type\Report {

	/**
	 * @return null|Project
	 */
	function getProject() {
		$id = $this->getAttribute('project_id');
		if ($id)
			return Project::findOne(['id' => $id]);
		return null;
	}
}