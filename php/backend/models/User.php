<?php
/**
 * Created by PhpStorm.
 * User: zachot
 * Date: 06.08.15
 * Time: 2:57
 */

namespace backend\models;


use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface {
	/**
	 * Finds an identity by the given ID.
	 *
	 * @param string|integer $id the ID to be looked for
	 * @return User|null the identity object that matches the given ID.
	 */
	public static function findIdentity($id) {
		return static::findOne($id);
	}


	/**
	 * Finds an identity by the given token.
	 *
	 * @param string $token the token to be looked for
	 * @return User|null the identity object that matches the given token.
	 */
	public static function findIdentityByAccessToken($token, $type = null) {
		return static::findOne(['token' => $token]);
	}

	/**
	 * @return int|string current user ID
	 */
	public function getId() {
		return $this->getAttribute('id');
	}

	/**
	 * @return string current user auth key
	 */
	public function getAuthKey() {
		return $this->getAttribute('token');
	}


	/**
	 * @param string $authKey
	 * @return boolean if auth key is valid for current user
	 */
	public function validateAuthKey($authKey) {
		return $this->getAuthKey() === $authKey;
	}

	/**
	 * @param $password
	 * @return bool
	 * @throws \yii\base\InvalidConfigException
	 */
	public function validatePassword($password) {
		return \Yii::$app->getSecurity()->validatePassword($password, $this->getAttribute("password"));
	}
}