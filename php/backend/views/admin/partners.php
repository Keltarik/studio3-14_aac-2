<?php
/**
 * @var $partners \backend\models\type\Partner[]
 */


$this->registerJsFile('@web/assets/angular.min.js', ['position' => $this::POS_END]);
$this->registerJsFile('@web/assets/ngDraggable.js', ['position' => $this::POS_END]);
$this->registerJsFile('@web/js/admin-angular.js', ['position' => $this::POS_END]);


?>
<div>
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Партнеры <a class="btn btn-sm" href="<?= \yii\helpers\Url::to('@web/edit/partner') ?>">+<i
						class="fa fa-user"></i> добавить</a>
			</h1>
		</div>
	</div>

	<div class="panel panel-default">

		<div class="panel-body">
			<table class="table table-striped table-bordered table-hover data-table">
				<thead>
				<tr>
					<th>Фото</th>
					<th>Имя</th>
					<th>Меню</th>
				</tr>
				</thead>
				<tfoot>
				<tr>
					<td colspan="3"><a class="btn btn-primary" href="<?= \yii\helpers\Url::to('@web/edit/partner?id=0') ?>">Добавить</a>
					</td>
				</tr>
				</tfoot>
				<tbody>
				<?php foreach ($partners as $partner) { ?>
					<tr>
						<td style="width: 100px; text-align: center;"><img class="img-responsive" src="<?= $partner->getLogoUrl() ?>"></td>
						<td>
							<h4><?= $partner->getAttribute('name') ?></h4>

						</td>
						<td>
							<div>
								<label for="">На странице</label>
								<a class="js-toggle"
									 href="<?= \yii\helpers\Url::to("@web/json/toggle-partner-visible?id=" . $partner->getAttribute('id')) ?>"
									 data-toggle-inner="<?= ($partner->getAttribute('visible')) ? "Скрыт -> Показать" : "Виден -> Скрыть" ?>"
									><?= ($partner->getAttribute('visible')) ? "Виден -> Скрыть" : "Скрыт -> Показать" ?></a>
							</div>

							<div>
								<div>Изменить</div>
								<a class="btn btn-sm btn-primary" href="<?= \yii\helpers\Url::to('@web/edit/partner?id=' . $partner->getAttribute('id')) ?>">Редактировать</a>

							</div>

						</td>
					</tr>
				<?php } ?>

				</tbody>
			</table>
		</div>
	</div>
</div>
