<?php
/**
 * @var $team \backend\models\type\Teammate[]
 */


$this->registerJsFile('@web/assets/angular.min.js', ['position' => $this::POS_END]);
$this->registerJsFile('@web/assets/ngDraggable.js', ['position' => $this::POS_END]);
$this->registerJsFile('@web/js/admin-angular.js', ['position' => $this::POS_END]);


?>
<div>
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Команда <a class="btn btn-sm" href="<?= \yii\helpers\Url::to('@web/edit/teammate') ?>">+<i
						class="fa fa-user"></i> добавить</a>
			</h1>
		</div>
	</div>

	<div class="panel panel-default">

		<div class="panel-body">
			<table class="table table-striped table-bordered table-hover data-table">
				<thead>
				<tr>
					<th>Фото</th>
					<th>Имя</th>
					<th>Меню</th>
				</tr>
				</thead>
				<tfoot>
				<tr>
					<td colspan="3"><a class="btn btn-primary" href="<?= \yii\helpers\Url::to('@web/edit/teammate?id=0') ?>">Добавить</a>
					</td>
				</tr>
				</tfoot>
				<tbody>
				<?php 
				$team = \backend\models\type\Teammate::find()->orderBy('position')->all();
				foreach ($team as $teammate) { 
				?>
					<tr>
						<td style="width: 100px; text-align: center;"><img class="img-responsive"
																															 src="<?= $teammate->getPhotoUrl() ?>"></td>
						<td>
							<h4><?= $teammate->getAttribute('name') ?></h4>

							<p><?= $teammate->getAttribute('profession') ?></p>

						</td>
						<td>
							<div>
								<label for="">На главной</label>
								<a class="js-toggle"
									 href="<?= \yii\helpers\Url::to("@web/json/toggle-person-main?id=" . $teammate->getAttribute('id')) ?>"
									 data-toggle-inner="<?= ($teammate->getAttribute('main')) ? "Скрыт -> Показать" : "Виден -> Скрыть" ?>"
									><?= ($teammate->getAttribute('main')) ? "Виден -> Скрыть" : "Скрыт -> Показать" ?></a>
							</div>
							<div>
								<label for="">Бывший сотрудник</label>
								<a class="js-toggle"
									 href="<?= \yii\helpers\Url::to("@web/json/toggle-person-past?id=" . $teammate->getAttribute('id')) ?>"
									 data-toggle-inner="<?= ($teammate->getAttribute('past')) ? "Сделать бывшим" : "Сделать настоящим" ?>"
									><?= ($teammate->getAttribute('past')) ? "Сделать настоящим" : "Сделать бывшим" ?></a>
							</div>

							<div>
								<div>Изменить</div>
								<a class="btn btn-sm btn-primary" href="<?= \yii\helpers\Url::to('@web/edit/teammate?id=' . $teammate->getAttribute('id')) ?>">Редактировать</a>
								
							</div>
							<div>
								<div>Позиция</div>
								<?php $moveUp = \yii\helpers\Url::to('@web/json/move-teammate-up?id=' . $teammate->getAttribute('id') . '&pos=' . $teammate->getAttribute('position')) ?>
								<?php $moveDown = \yii\helpers\Url::to('@web/json/move-teammate-down?id=' . $teammate->getAttribute('id') . '&pos=' . $teammate->getAttribute('position')) ?>
								<a class="btn btn-sm btn-default <?= ($teammate->getAttribute('position') == 1) ? "disabled" : "" ?>" href="<?= $moveUp ?>">Вверх</a>
								<a class="btn btn-sm btn-warning <?= ($teammate->getAttribute('position') == count($team)) ? "disabled" : "" ?>" href="<?= $moveDown?>">Вниз</a>
							</div>

						</td>
					</tr>
				<?php } ?>

				</tbody>
			</table>
		</div>
	</div>
</div>
