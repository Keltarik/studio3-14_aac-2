<?php
/**
 * @var $model \backend\models\edit\EditProject
 */
use yii\helpers\Url;

?>

<div>


	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Проект</h1>
		</div>
	</div>

	<?php $form = \yii\widgets\ActiveForm::begin(['options' => ['class' => 'form-project form', 'enctype' => "multipart/form-data"]]); ?>

	<?php if ($model->hasErrors()) { ?>
		<div>
			<div class="alert alert-danger">Error</div>
		</div>
	<?php } ?>

	<div class="row">
		<div class="col-md-9">
			<div class="panel panel-default">
				<div class="panel-heading">Общее</div>
				<div class="panel-body">


					<?= $form->field($model, 'title')
						->textInput(
							['placeholder' => 'Заголовок',
								'maxlength' => 255,
								'class' => 'form-control']
						)->label('Заголовок') ?>


					<?= $form->field($model, 'address')
						->textInput(
							['placeholder' => 'Адрес',
								'class' => 'form-control']
						)->label('Адрес') ?>

					<?= $form->field($model, 'text')->widget(
						\dosamigos\tinymce\TinyMce::className(),
						[
							'options' => ['rows' => 20],
							'language' => 'ru',
							'clientOptions' => [
								"relative_urls" => false,
								"convert_urls" => false,
								"paste_auto_cleanup_on_paste" => true,
								"paste_remove_styles" => true,
								"paste_remove_spans" => true,
								"paste_strip_class_attributes" => 'none',
								"style_formats" => [
									["title" => 'Заголовок с красной точкой', "block" => 'h5', "classes" => "page__title"],
									["title" => 'Заголовок', "block" => 'h2', "classes" => ""],
									["title" => 'Параграф', "block" => 'p', "classes" => "page__text"],
								],
								'content_css' => [

									Url::to('@webUrl/assets/bootstrap.css'),
									Url::to('@web/css/style.css'),
								],
								'menu' => [
									'edit' => ['title' => 'Edit', 'items' => 'undo redo | cut copy paste pastetext | selectall'],
									'insert' => ['title' => 'Insert', 'items' => 'link media | template hr'],
									'view' => ['title' => 'View', 'items' => 'visualaid'],
									'format' => ['title' => 'Format', 'items' => 'bold italic underline strikethrough superscript subscript | formats | removeformat'],
									'tools' => ['title' => 'Tools', 'items' => 'spellchecker code']
								],
								'plugins' => [
									"advlist autolink link charmap print preview anchor",
									"searchreplace visualblocks code fullscreen",
									"insertdatetime media table contextmenu paste example fullscreen"
								],
								'toolbar' => "undo redo | styleselect | bold italic | link image | example | fullscreen"
							]
						]
					)->label('Описание проекта'); ?>


				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">Команда</div>
				<div class="panel-body">
					<table class="table table-responsive table-striped">
						<thead>
						<tr>
							<th>№</th>
							<th>Сотрудник</th>
							<th>Роль в проекте</th>
						</tr>
						</thead>
						<tfoot>
						<tr>
							<td colspan="2"><a href="#teammatelist" data-add="teammate"
																 class="btn btn-primary btn-add-person">Добавить</a></td>
							<td colspan="1"><a href="<?= Url::to('@web/edit/teammate?id=0') ?>">Добавить нового сотрудника</a></td>
						</tr>
						</tfoot>
						<tbody id="teammatelist">
						<?php
						/**
						 * @var $team \backend\models\type\Teammate[];
						 */
						$team = \backend\models\type\Teammate::find()->orderBy('name')->all();
						$new_team = [];
						foreach ($team as $teammate) {
							$new_team[$teammate->getAttribute('id')] = $teammate->getPhotoUrl();
						}
						foreach ($model->team as $index => $t) { ?>
							<tr>
								<td style="vertical-align: middle;">
									<button class="btn btn-sm btn-person-up">Up</button>
									<button class="btn btn-sm btn-info btn-person-down">Down</button>
									<button class="btn btn-sm btn-danger btn-person-delete">Delete</button>
								</td>
								<td>
									<div class="row">
										<div class="col-xs-4" style="width: 100px;">
											<img src="<?= $new_team[$t] ?>" alt="" class="img-responsive">
										</div>
										<div class="col-xs-8">

											<div class="form-group field-editproject-team">
												<label class="control-label" for="editproject-team"></label>
												<select id="editproject-team" class="form-control change-person" name="EditProject[team][]">
													<?php foreach ($team as $teammate) { ?>
														<option
															value="<?= $teammate->getAttribute('id') ?>"
															<?= ($teammate->getAttribute('id') == $t) ? " selected" : "" ?>
															><?= $teammate->getAttribute('name') ?></option>
													<?php } ?>
												</select>
											</div>
										</div>
									</div>
								</td>
								<td>
									<div class="form-group field-editproject-role">
										<label class="control-label" for="editproject-role"></label>
										<input type="text" id="editproject-role"
													 class="form-control"
													 name="EditProject[role][]"
													 value="<?= $model->role[$index] ?>"
													 maxlength="255" placeholder="Роль">

										<div class="help-block"></div>
									</div>
								</td>
							</tr>
						<?php } ?>

						</tbody>
					</table>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">Комнаты</div>
				<div class="panel-body">
					<table class="table table-responsive table-striped">
						<thead>
						<tr>
							<td style="width: 200px;">№</td>
							<td>Название</td>
						</tr>
						</thead>
						<tfoot>
						<tr>
							<td colspan="2">
								<a href="<?= Url::to('@web/json/add-room') ?>" class="add-room">Добавить комнату</a>
							</td>
						</tr>
						</tfoot>
						<tbody id="project_rooms">

						<?php
						foreach ($model->room_ids as $room_id) {
							/**
							 * @var $room \backend\models\type\Room
							 */
							$room = \backend\models\type\Room::findOne(['id' => $room_id]);
							if ($room) {
								?>
								<tr id="room_<?= $room_id ?>">
									<td style="vertical-align: middle;">
										<button class="btn btn-sm btn-person-up">Up</button>
										<button class="btn btn-sm btn-info btn-person-down">Down</button>
										<button class="btn btn-sm btn-danger btn-person-delete">Delete</button>
									</td>
									<td>

										<input type="hidden" name="EditProject[room_ids][]" value="<?= $room_id ?>">
										<h4><?= $room->getAttribute('title') ?></h4>
										<a href="<?= Url::to('@web/json/add-room?id=' . $room_id) ?>" class="edit-room">Редактировать</a>
									</td>
								</tr>
								<?php
							}
						}
						?>


						</tbody>
					</table>
				</div>
			</div>

            <div class="panel panel-default">
                <div class="panel-heading">Партнеры</div>
                <div class="panel-body">
                    <table class="table table-responsive table-striped">
                        <thead>
                        <tr>
                            <th style="width: 200px;">№</th>
                            <th>
                            <div class="row">
                                <div class="col-xs-4">Логотип</div>
                                <div class="col-xs-8">Название</div>
                            </div>

                            </th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <td colspan="3"><a href="#project_partners" data-add="partner"
                                               class="btn btn-primary btn-add-partner">Добавить</a></td>

                        </tr>
                        </tfoot>
                        <tbody id="project_partners">

                        <?php
                        /**
                         * @var $partners \backend\models\type\Partner[];
                         */
                        $partners = \backend\models\type\Partner::find()->orderBy('name')->all();
                        foreach ($model->partner_ids as $partner_id) {
                            /**
                             * @var $partner \backend\models\type\Partner
                             */
                            $partner = \backend\models\type\Partner::findOne(['id' => $partner_id]);
                            if ($partner) {
                                ?>
                                <tr id="partner_<?= $partner_id ?>">
                                    <td style="vertical-align: middle;">
                                        <button class="btn btn-sm btn-danger btn-partner-delete">Delete</button>
                                    </td>
                                    <td>
                                        <div class="row">
                                            <div class="col-xs-4" style="width: 100px;">
                                                <img src="<?= $partner->getLogoUrl() ?>" alt="" class="img-responsive">
                                            </div>
                                            <div class="col-xs-8">

                                                <div class="form-group field-editproject-partner">
                                                    <label class="control-label" for="editproject-partner"></label>
                                                    <select id="editproject-partner" class="form-control change-partner" name="EditProject[partner_ids][]">
                                                        <?php foreach ($partners as $p) { ?>
                                                            <option
                                                                value="<?= $p->getAttribute('id') ?>"
                                                                <?= ($p->getAttribute('id') == $partner_id) ? " selected" : "" ?>
                                                                ><?= $p->getAttribute('name') ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </td>

                                </tr>
                                <?php
                            }
                        }
                        ?>


                        </tbody>
                    </table>
                </div>
            </div>

			<div class="panel panel-default">
				<div class="panel-heading">Отзывы</div>
				<div class="panel-body">

					<table class="table table-responsive table-striped">
						<thead>
						<tr>
							<td style="width: 200px;">№</td>
							<td>Автор</td>
							<td>Отзыв</td>
						</tr>
						</thead>
						<tfoot>
						<tr>
							<td colspan="2">
								<a href="<?= Url::to('@web/json/add-review') ?>" class="add-room">Добавить Отзыв</a>
							</td>
						</tr>
						</tfoot>
						<tbody id="project_reviews">

						<?php
						foreach ($model->review_ids as $review_id) {
							/**
							 * @var $review \backend\models\type\Review
							 */
							$review = \backend\models\type\Review::findOne(['id' => $review_id]);
							if ($review) {
								/**
								 * @var $photo \common\models\type\Attachment
								 */
								$photo = \common\models\type\Attachment::findOne(['id' => $review->getAttribute('author_photo')]);
								?>
								<tr id="review_<?= $review_id ?>">
									<td style="vertical-align: middle;">
										<button class="btn btn-sm btn-person-up">Up</button>
										<button class="btn btn-sm btn-info btn-person-down">Down</button>
										<button class="btn btn-sm btn-danger btn-person-delete">Delete</button>
									</td>
									<td style="width: 300px;">

										<input type="hidden" name="EditProject[review_ids][]" value="<?= $review_id ?>">

										<div class="row">
											<div class="col-xs-4">
												<img src="<?= $photo->getUrl() ?>" height="50" class="img-responsive" alt="">
											</div>
											<div class="col-xs-8">
												<h4><?= $review->getAttribute('author_name') ?></h4>

												<div class="pr"><?= $review->getAttribute('author_profession') ?></div>
											</div>
										</div>
										<a href="<?= Url::to('@web/json/add-review?id=' . $review_id) ?>"
											 class="edit-room">Редактировать</a>
									</td>
									<td style="font-style: italic; font-size: 10px;">
										<div class="review_text">
											<?= $review->getAttribute('text') ?>
										</div>
									</td>
								</tr>
								<?php
							}
						}
						?>


						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading">
					Меню
				</div>
				<div class="panel-body">
					<?= $form->field($model, 'titlepage')
						->textInput(
							['placeholder' => 'Заголовок окна',
								'maxlength' => 255,
								'class' => 'form-control input-sm']
						)->label('Заголовок окна') ?>

					<?= $form->field($model, 'slug')
						->textInput(
							['placeholder' => 'Путь в url',
								'maxlength' => 255,
								'class' => 'form-control input-sm']
						)->label('Путь в url') ?>
                    <?= $form->field($model, 'tags_str')
                        ->textInput(
                            ['placeholder' => 'Теги через запятую',
                                'maxlength' => 255,
                                'class' => 'form-control input-sm']
                        )->label('Теги') ?>

					<input type="submit" class="btn btn-primary" value="Сохранить">
					<a class="pull-right" target="_blank" href="<?= Url::to('@webUrl/project/' . $model->slug) ?>">Перейти на
						сайт</a>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">Изображения</div>
				<div class="panel-body">
					<div class="form-group field-editproject-thumbnail required">
						<?= $form->field($model, "thumbnail")->hiddenInput(['class' => 'hidden-attachment'])->label(false) ?>
						<label class="control-label" for="editproject-thumbnail">Главный эскиз</label>
						<?php
						/**
						 * @var $att \common\models\type\Attachment
						 */
						$att = \common\models\type\Attachment::findOne(['id' => $model->thumbnail])
						?>
						<div class="file">
							<div class="<?= ($att) ? "" : "hide" ?> preview">
								<img src="<?= ($att) ? $att->getUrl() : "" ?>" alt="" class="img-responsive">
							</div>
							<div class="input">
								<input type="file"
											 id="editproject-thumbnail"
											 class="upload-file"
											 name="Upload[thumbnail]"
											 data-name="thumbnail">
							</div>
						</div>
					</div>
					<div class="form-group field-editproject-thumbnailback required">
						<label class="control-label" for="editproject-thumbnailback">Размытый эскиз</label>
						<?= $form->field($model, "thumbnailback")->hiddenInput(['class' => 'hidden-attachment'])->label(false) ?>
						<?php
						/**
						 * @var $att \common\models\type\Attachment
						 */
						$att = \common\models\type\Attachment::findOne(['id' => $model->thumbnailback])
						?>
						<div class="file">
							<div class="<?= ($att) ? "" : "hide" ?> preview">
								<img src="<?= ($att) ? $att->getUrl() : "" ?>" alt="" class="img-responsive">
							</div>
							<div class="input">
								<input type="file"
											 id="editproject-thumbnailback"
											 class="upload-file"
											 name="Upload[thumbnailback]"
											 data-name="thumbnailback">
							</div>
						</div>

					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">Мета данные</div>
				<div class="panel-body">
					<?= $form->field($model, 'social_title')
						->textInput(
							['placeholder' => 'title',
								'maxlength' => 255,
								'class' => 'form-control']
						)->label('Мета заголовок') ?>

					<div class="form-group field-editproject-social_image required">
						<?= $form->field($model, "social_image")->hiddenInput(['class' => 'hidden-attachment'])->label(false) ?>
						<label class="control-label" for="editproject-social_image">Картинка для соцсетей (квадрат до
							400x400)</label>
						<?php
						/**
						 * @var $att \common\models\type\Attachment
						 */
						$att = \common\models\type\Attachment::findOne(['id' => $model->social_image])
						?>
						<div class="file">
							<div class="<?= ($att) ? "" : "hide" ?> preview">
								<img src="<?= ($att) ? $att->getUrl() : "" ?>" alt="" class="img-responsive">
							</div>
							<div class="input">
								<input type="file"
											 id="editproject-social_image"
											 class="upload-file"
											 name="Upload[social_image]"
											 data-name="social_image">
							</div>
						</div>
					</div>

					<?= $form->field($model, 'social_description')
						->textarea(
							['placeholder' => 'descriptin',
								'rows' => 6,
								'class' => 'form-control']
						)->label('Мета описание') ?>
				</div>
			</div>
		</div>
	</div>
	<?php \yii\widgets\ActiveForm::end(); ?>

</div>
