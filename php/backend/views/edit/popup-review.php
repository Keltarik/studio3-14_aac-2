<?php
/**
 * @var $model \backend\models\edit\EditReview
 */
?>
<div id="review_inner">

	<div style="margin: 30px auto; max-width: 600px; background: white; position: relative; padding: 20px;">
		<?php $form = \yii\widgets\ActiveForm::begin(['options' => ['class' => 'form-review form', 'enctype' => "multipart/form-data"]]); ?>

		<?php if ($model->hasErrors()) { ?>
			<div>
				<div class="alert alert-danger">Error</div>
			</div>
		<?php } ?>

		<input type="hidden" name="id" value="<?=$model->id?>">

		<div class="form-group field-editreview-author_photo required">
			<?= $form->field($model, "author_photo")->hiddenInput(['class' => 'hidden-attachment'])->label(false) ?>
			<label class="control-label" for="editreview-author_photo">Фото автора</label>
			<?php
			/**
			 * @var $att \common\models\type\Attachment
			 */
			$att = \common\models\type\Attachment::findOne(['id' => $model->author_photo])
			?>
			<div class="file">
				<div class="<?= ($att) ? "" : "hide" ?> preview">
					<img src="<?= ($att) ? $att->getUrl() : "" ?>" alt="" class="img-responsive">
				</div>
				<div class="input">
					<input type="file"
								 id="editreview-author_photo"
								 class="upload-file"
								 name="Upload[author_photo]"
								 data-name="author_photo">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-6">
				<?= $form->field($model, 'author_name')
					->textInput(
						['placeholder' => 'Имя автора',
							'maxlength' => 255,
							'class' => 'form-control']
					)->label('Имя автора') ?>
			</div>
			<div class="col-xs-6">
				<?= $form->field($model, 'author_profession')
					->textInput(
						['placeholder' => 'Профессия автора',
							'maxlength' => 255,
							'class' => 'form-control']
					)->label('Профессия автора') ?>
			</div>
		</div>

		<?= $form->field($model, 'video')
			->textInput(
				['placeholder' => 'Youtube ссылка',
					'maxlength' => 255,
					'class' => 'form-control']
			)->label('Видео ссылка youtube') ?>

		<?php
		$id = 'edit-review__' . time();
		?>

		<?= $form->field($model, 'text')->widget(
			\dosamigos\tinymce\TinyMce::className(),
			[

				'options' => ['rows' => 20, 'id' => $id],
				'language' => 'ru',
				'clientOptions' => [
					"style_formats" => [
						["title" => 'Заголовок с красной точкой', "block" => 'h5', "classes" => "page__title"],
					],
					'content_css' => [

						\yii\helpers\Url::to('@webUrl/assets/bootstrap.css'),
//									Url::to('@webUrl/style/index.css'),
						\yii\helpers\Url::to('@web/css/style.css'),
					],
					'menu' => [
						'edit' => ['title' => 'Edit', 'items' => 'undo redo | cut copy paste pastetext | selectall'],
						'insert' => ['title' => 'Insert', 'items' => 'link media | template hr'],
						'view' => ['title' => 'View', 'items' => 'visualaid'],
						'format' => ['title' => 'Format', 'items' => 'bold italic underline strikethrough superscript subscript | formats | removeformat'],
						'tools' => ['title' => 'Tools', 'items' => 'spellchecker code']
					],
					'plugins' => [
						"advlist autolink link charmap print preview anchor",
						"searchreplace visualblocks code fullscreen",
						"insertdatetime media table contextmenu paste example"
					],
					'toolbar' => "undo redo | styleselect | bold italic | link image | example"
				]
			]
		)->label('Описание комнаты'); ?>


		<div>
			<h4>Для главной страницы</h4>

			<?= $form->field($model, 'main_title')
				->textInput(
					['placeholder' => 'Название проекта',
						'maxlength' => 255,
						'class' => 'form-control']
				)->label('Сокращенное название проекта') ?>

			<?= $form->field($model, 'main_text')
				->textarea(
					['placeholder' => 'Текст',
						'rows' => 10,
						'class' => 'form-control']
				)->label('Мини отзыв для главной страницы') ?>
		</div>
		<script>
			jQuery(document).ready(function () {
				tinyMCE.execCommand('mceAddEditor', false, "<?=$id?>");

				$('#editroom-text').parents('form').on('beforeValidate', function () {
					tinymce.triggerSave();
				});
			});
		</script>

		<input type="submit" class="btn btn-success" value="Применить">
		<input type="submit" name="close" class="btn btn-primary" onclick="saveAndClose();" value="Применить и закрыть">
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
		<?php \yii\widgets\ActiveForm::end(); ?>
	</div>
</div>
