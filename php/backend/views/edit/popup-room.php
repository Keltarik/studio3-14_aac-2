<?php
/**
 * @var $model \backend\models\edit\EditRoom
 */
?>
<div id="room_inner">

	<div style="margin: 30px auto; max-width: 600px; background: white; position: relative; padding: 20px;">
		<?php $form = \yii\widgets\ActiveForm::begin(['options' => ['class' => 'form-room form', 'enctype' => "multipart/form-data"]]); ?>

		<?php if ($model->hasErrors()) { ?>
			<div>
				<div class="alert alert-danger">Error</div>
			</div>
		<?php } ?>

		<input type="hidden" name="id" value="<?=$model->id?>">

		<?= $form->field($model, 'title')
			->textInput(
				['placeholder' => 'Заголовок',
					'maxlength' => 255,
					'class' => 'form-control']
			)->label('Заголовок') ?>

		<div class="row">
			<div class="col-xs-6">
				<?= $form->field($model, 'menu_title')
					->textInput(
						['placeholder' => 'Заголовок в меню',
							'maxlength' => 100,
							'class' => 'form-control']
					)->label('Заголовок в меню') ?>
			</div>
			<div class="col-xs-6">
				<?= $form->field($model, 'anchor')
					->textInput(
						['placeholder' => 'Якорная ссылка',
							'maxlength' => 100,
							'class' => 'form-control']
					)->label('Ссылка для перехода') ?>
				<p style="font-size: 10px;">Слово англискими буквами (#kitchen) без решетки, будет добавляться к сылке проекта при переходе к комнате без js или для создания ссылок внутри страницы</p>
			</div>
		</div>

		<?php
		$id = 'edit-room__' . time();
		?>

		<?= $form->field($model, 'text')->widget(
			\dosamigos\tinymce\TinyMce::className(),
			[

				'options' => ['rows' => 20, 'id' => $id],
				'language' => 'ru',
				'clientOptions' => [
					"style_formats" => [
						["title" => 'Заголовок с красной точкой', "block" => 'h5', "classes" => "page__title"],
					],
					'content_css' => [

						\yii\helpers\Url::to('@webUrl/assets/bootstrap.css'),
//									Url::to('@webUrl/style/index.css'),
						\yii\helpers\Url::to('@web/css/style.css'),
					],
					'menu' => [
						'edit' => ['title' => 'Edit', 'items' => 'undo redo | cut copy paste pastetext | selectall'],
						'insert' => ['title' => 'Insert', 'items' => 'link media | template hr'],
						'view' => ['title' => 'View', 'items' => 'visualaid'],
						'format' => ['title' => 'Format', 'items' => 'bold italic underline strikethrough superscript subscript | formats | removeformat'],
						'tools' => ['title' => 'Tools', 'items' => 'spellchecker code']
					],
					'plugins' => [
						"advlist autolink link charmap print preview anchor",
						"searchreplace visualblocks code fullscreen",
						"insertdatetime media table contextmenu paste example"
					],
					'toolbar' => "undo redo | styleselect | bold italic | link image | example"
				]
			]
		)->label('Описание комнаты'); ?>

		<script>
			jQuery(document).ready(function () {
				tinyMCE.execCommand('mceAddEditor', false, "<?=$id?>");

				$('#editroom-text').parents('form').on('beforeValidate', function () {
					tinymce.triggerSave();
				});
			});
		</script>

		<input type="submit" class="btn btn-success" value="Применить">
		<input type="submit" name="close" class="btn btn-primary" onclick="saveAndClose();" value="Применить и закрыть">
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
		<?php \yii\widgets\ActiveForm::end(); ?>
	</div>
</div>
