<?php
/**
 * Created by PhpStorm.
 * User: zachot
 * Date: 02.08.15
 * Time: 4:33
 */

/**
 * @var $model \backend\models\edit\EditTeammate
 */
use yii\helpers\Url;
?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Участник</h1>
	</div>
</div>


<div class="row">
	<?php $form = \yii\widgets\ActiveForm::begin(['options' => ['class' => 'form-teammate form', 'enctype' => "multipart/form-data"]]); ?>

	<?php if ($model->hasErrors()) { ?>
		<div>
			<div class="alert alert-danger">Error</div>
		</div>
	<?php } ?>

	<div class="row">
		<div class="col-md-9">
			<div class="panel panel-default">

				<div class="panel-body">


					<div class="row">
						<div class="col-xs-4">
							<div class="form-group field-editproject-photo required">

								<?= $form->field($model, "photo")->hiddenInput(['class' => 'hidden-attachment'])->label(false) ?>
								<label class="control-label" for="editproject-photo">Фото</label>
								<?php
								/**
								 * @var $att \common\models\type\Attachment
								 */
								$att = \common\models\type\Attachment::findOne(['id' => $model->photo])
								?>
								<div class="file">
									<div class="<?= ($att) ? "" : "hide" ?> preview">
										<img src="<?= ($att) ? $att->getUrl() : "" ?>" alt="" class="img-responsive">
									</div>
									<div class="input">
										<input type="file"
													 id="editproject-photo"
													 class="upload-file"
													 name="Upload[photo]"
													 data-name="photo">
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-8">
							<?= $form->field($model, 'name')
								->textInput(
									['placeholder' => 'Имя',
										'maxlength' => 255,
										'class' => 'form-control']
								)->label('Имя') ?>


							<?= $form->field($model, 'profession')
								->textInput(
									['placeholder' => 'Профессия',
										'maxlength' => 255,
										'class' => 'form-control']
								)->label('Профессия') ?>
						</div>
					</div>

					<div>
						
						<?= $form->field($model, 'about')->widget(
						\dosamigos\tinymce\TinyMce::className(),
						[
							'options' => ['rows' => 20],
							'language' => 'ru',
							'clientOptions' => [
								"relative_urls" => false,
								"convert_urls" => false,
								"paste_auto_cleanup_on_paste" => true,
								"paste_remove_styles" => true,
								"paste_remove_spans" => true,
								"paste_strip_class_attributes" => 'none',
								"style_formats" => [
									["title" => 'Заголовок с красной точкой', "block" => 'h5', "classes" => "page__title"],
									["title" => 'Заголовок', "block" => 'h2', "classes" => ""],
									["title" => 'Параграф', "block" => 'p', "classes" => "page__text"],
								],
								'content_css' => [

									Url::to('@webUrl/assets/bootstrap.css'),
									Url::to('@web/css/style.css'),
								],
								'menu' => [
									'edit' => ['title' => 'Edit', 'items' => 'undo redo | cut copy paste pastetext | selectall'],
									'insert' => ['title' => 'Insert', 'items' => 'link media | template hr'],
									'view' => ['title' => 'View', 'items' => 'visualaid'],
									'format' => ['title' => 'Format', 'items' => 'bold italic underline strikethrough superscript subscript | formats | removeformat'],
									'tools' => ['title' => 'Tools', 'items' => 'spellchecker code']
								],
								'plugins' => [
									"advlist autolink link charmap print preview anchor",
									"searchreplace visualblocks code fullscreen",
									"insertdatetime media table contextmenu paste example fullscreen"
								],
								'toolbar' => "undo redo | styleselect | bold italic | link image | example | fullscreen"
							]
						]
					)->label('О сотруднике'); ?>
					</div>

				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading">
					Меню
				</div>
				<div class="panel-body">

					<?= $form->field($model, 'main')
						->checkbox(
							['uncheck' => '0', 'label' => 'Показывать на главной странице']
						)->label('Для главной') ?>
					<?= $form->field($model, 'past')
						->checkbox(
							['uncheck' => '0', 'label' => 'Перевести в статус бывшего']
						)->label('Бывший сотрудник') ?>

					<input type="submit" class="btn btn-primary" value="Сохранить">
				</div>
			</div>

		</div>
	</div>

	<?php \yii\widgets\ActiveForm::end(); ?>
</div>