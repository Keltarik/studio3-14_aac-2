<?php
/**
 * Created by PhpStorm.
 * User: zachot
 * Date: 02.08.15
 * Time: 4:33
 */

/**
 * @var $model \backend\models\edit\EditTeammate
 */
?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Участник</h1>
	</div>
</div>


<div class="row">
	<?php $form = \yii\widgets\ActiveForm::begin(['options' => ['class' => 'form-teammate form', 'enctype' => "multipart/form-data"]]); ?>

	<?php if ($model->hasErrors()) { ?>
		<div>
			<div class="alert alert-danger">Error</div>
		</div>
	<?php } ?>

	<div class="row">
		<div class="col-md-9">
			<div class="panel panel-default">

				<div class="panel-body">


					<div class="row">
						<div class="col-xs-4">
							<div class="form-group field-editproject-photo required">

								<?= $form->field($model, "photo")->hiddenInput(['class' => 'hidden-attachment'])->label(false) ?>
								<label class="control-label" for="editproject-photo">Фото</label>
								<?php
								/**
								 * @var $att \common\models\type\Attachment
								 */
								$att = \common\models\type\Attachment::findOne(['id' => $model->photo])
								?>
								<div class="file">
									<div class="<?= ($att) ? "" : "hide" ?> preview">
										<img src="<?= ($att) ? $att->getUrl() : "" ?>" alt="" class="img-responsive">
									</div>
									<div class="input">
										<input type="file"
													 id="editproject-photo"
													 class="upload-file"
													 name="Upload[photo]"
													 data-name="photo">
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-8">
							<?= $form->field($model, 'name')
								->textInput(
									['placeholder' => 'Имя',
										'maxlength' => 255,
										'class' => 'form-control']
								)->label('Имя') ?>


							<?= $form->field($model, 'profession')
								->textInput(
									['placeholder' => 'Профессия',
										'maxlength' => 255,
										'class' => 'form-control']
								)->label('Профессия') ?>
						</div>
					</div>

					<div>
						<?= $form->field($model, 'about')
							->textarea(
								['placeholder' => 'О сотруднике',
									'rows' => '10',
									'class' => 'form-control']
							)->label('О сотруднике') ?>
					</div>

				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading">
					Меню
				</div>
				<div class="panel-body">
					<input type="submit" class="btn btn-primary" value="Сохранить">
				</div>
			</div>

		</div>
	</div>

	<?php \yii\widgets\ActiveForm::end(); ?>
</div>