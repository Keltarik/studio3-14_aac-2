<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="<?= \yii\helpers\Url::to('@web/') ?>">Admin Studio 3.14</a>
	</div>
	<!-- /.navbar-header -->

	<ul class="nav navbar-top-links navbar-right">
		<li>
			<a href="<?= \yii\helpers\Url::to("@webUrl") ?>">
				Перейти на сайт
			</a>
		</li>
		<li>
			<a href="<?= \yii\helpers\Url::to("@web/logout") ?>">
				Выйти
			</a>
		</li>
	</ul>
	<!-- /.navbar-top-links -->

	<div class="navbar-default sidebar" role="navigation">
		<div class="sidebar-nav navbar-collapse">
			<ul class="nav" id="side-menu">
				<li class="sidebar-search">

					<!-- /input-group -->
				</li>
				<li>
					<a href="<?= \yii\helpers\Url::to("@web/") ?>"><i class="fa fa-home fa-fw"></i> Главная</a>
				</li>
                <li>
                    <a href="<?= \yii\helpers\Url::to("@web/slider") ?>"><i class="fa fa-photo fa-fw"></i> Слайдер</a>
                </li>
				<li>
					<a href="<?= \yii\helpers\Url::to("@web/projects") ?>"><i class="fa fa-briefcase fa-fw"></i> Проекты</a>
				</li>
                <li>
                    <a href="<?= \yii\helpers\Url::to("@web/tags") ?>"><i class="fa fa-tags fa-fw"></i> Теги</a>
                </li>
				<li>
					<a href="<?= \yii\helpers\Url::to("@web/team") ?>"><i class="fa fa-users fa-fw"></i> Команда</a>
				</li>
                <li>
                    <a href="<?= \yii\helpers\Url::to("@web/partners") ?>"><i class="fa fa-building fa-fw"></i> Партнеры</a>
                </li>
				<li>
					<a href="<?= \yii\helpers\Url::to("@web/report") ?>"><i class="fa fa-users fa-fw"></i> Сообщения с сайта</a>
				</li>

			</ul>
		</div>
		<!-- /.sidebar-collapse -->
	</div>
	<!-- /.navbar-static-side -->
</nav>
