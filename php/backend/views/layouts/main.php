<?php

/**
 * @var $content string
 */
use yii\helpers\Html;

\backend\assets\AdminAssets::register($this);
\backend\assets\AdminLTIE9::register($this);
\backend\assets\AdminFootAssets::register($this);
?>
<?php $this->beginPage() ?>

<!DOCTYPE html>

<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?= Html::encode($this->title) ?></title>
	<link rel="icon" href="<?= \yii\helpers\Url::to('@web/favicon.ico') ?>"/>

	<!--	<link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">-->
	<!--	<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">-->
	<!--	<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">-->
	<!--	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">-->


	<?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
<?php
if (isset($this->params['clear'])) {
	?>
	<?= $content; ?>
<?php } else { ?>
	<div id="wrapper">
		<!-- Navigation -->

		<?= $this->renderFile('@app/views/layouts/nav.php') ?>
		<div id="page-wrapper">
			<?= $content; ?>
		</div>
		<!-- /#page-wrapper -->

	</div>
<?php } ?>



<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
