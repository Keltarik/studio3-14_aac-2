<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>"/>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrapper" style="margin: 0 auto; width: 500px;">

	<div style="margin:50px 0 10px; border: 1px solid #DDDDDD; border-radius: 3px;">
		<div class="text" style="margin: 0 0 50px;font-size: 16px;color:#000;">
			<?= $content ?>
		</div>
	</div>
	<div style="font-size: 14px; text-align: center;margin-bottom: 50px"><a href="http://studio3-14.ru">Studia 314</a></div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
