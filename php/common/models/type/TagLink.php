<?php
/**
 * @property integer $tag_id
 * @property integer $project_id
 * @property integer $id
 */
namespace common\models\type;


use yii\db\ActiveRecord;

class TagLink extends ActiveRecord {

    public static function tableName()
    {
        return '{{%tag_link}}';
    }
}