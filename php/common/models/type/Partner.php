<?php
/**
 * Created by PhpStorm.
 * User: zachot
 * Date: 05.08.15
 * Time: 12:34
 */

namespace common\models\type;


use yii\db\ActiveRecord;

class Partner extends ActiveRecord {

    /**
     * @return string
     */
    public function getLogoUrl() {
        return $this->__getUrl($this->getAttribute('logo'));
    }

    /**
     * @param $id
     * @return string
     */
    private function __getUrl($id) {
        /**
         * @var $_att Attachment
         */
        $_att = Attachment::findOne(['id' => $id]);
        if ($_att)
            return $_att->getUrl();
        return "";
    }

}