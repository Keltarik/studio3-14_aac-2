<?php
/**
 * Created by PhpStorm.
 * User: zachot
 * Date: 01.08.15
 * Time: 21:52
 */

namespace common\models\type;


use common\models\type\Attachment;
use common\models\type\Review;
use yii\db\ActiveRecord;

class Project extends ActiveRecord {
	/**
	 * @return string
	 */
	public function getBackBlurAttachmentUrl() {
		return $this->__getUrl($this->getAttribute('thumbnail-back'));
	}

	/**
	 * @return string
	 */
	public function getThumbnailAttachmentUrl() {
		return $this->__getUrl($this->getAttribute('thumbnail'));
	}

	/**
	 * @param $id
	 * @return string
	 */
	private function __getUrl($id) {
		/**
		 * @var $_att Attachment
		 */
		$_att = Attachment::findOne(['id' => $id]);
		if ($_att)
			return $_att->getUrl();
		return "";
	}

	public function getTeam() {
		$team = $this->getAttribute('team');
		if (!$team)
			return [];
		return unserialize($team);
	}

	/**
	 * @return array|Room[]
	 */
	public function getRooms() {
		$ids = $this->getAttribute('room-ids');
		if (!trim($ids))
			return [];
		$i = explode(',', $ids);

		$rooms = Room::find()
			->where('`id` IN (' . $ids . ')')
			->all();

		$r = [];

		foreach ($i as $index => $key) {
			foreach ($rooms as $room) {
				if ($room->getAttribute('id') == $key) {
					$r[$index] = $room;
					break;
				}
			}
		}

		return $r;
	}

    /**
     * @return array|Partner[]
     */
    public function getPartners() {
        $ids = $this->getAttribute('partner-ids');
        if (!trim($ids))
            return [];
        $i = explode(',', $ids);

        $partners = Partner::find()
            ->where('`id` IN (' . $ids . ')')
            ->all();

        $r = [];

        foreach ($i as $index => $key) {
            foreach ($partners as $partner) {
                if ($partner->getAttribute('id') == $key) {
                    $r[$index] = $partner;
                    break;
                }
            }
        }

        return $r;
    }

    public function  getTags() {
        $tags = $this->getAttribute('tags');

        if (!trim($tags))
            return [];

        return $tags;
    }


	/**
	 * @return array|Review[]
	 */
	public function getReviews() {
		$ids = $this->getAttribute('review-ids');
		if (!trim($ids) || empty($ids))
			return [];

		$i = explode(',', $ids);

		$reviews = Review::find()
			->where('`id` IN (' . $ids . ')')
			->all();

		$r = [];

		foreach ($i as $index => $key) {
			foreach ($reviews as $review) {
				if ($review->getAttribute('id') == $key) {
					$r[$index] = $review;
					break;
				}
			}
		}

		return $r;
	}

	public function getSocial() {
		$s = $this->getAttribute('social');
		if (!$s)
			return [$this->getAttribute('title-page'), $this->getThumbnailAttachmentUrl(), ''];

		$social = unserialize($s);
		$ret = [];

		$ret[] = ($social && isset($social['title'])) ? $social['title'] : $this->getAttribute('title-page');
		$ret[] = ($social && isset($social['image'])) ? $this->__getImageUrl($social['image']) : $this->getThumbnailAttachmentUrl();
		$ret[] = ($social && isset($social['description'])) ? $social['description'] : '';

		return $ret;
	}

	private function __getImageUrl($id) {
		/**
		 * @var $_att \common\models\type\Attachment
		 */
		$_att = Attachment::findOne($id);
		if ($_att)
			return $_att->getUrl();
		return $this->getThumbnailAttachmentUrl();
	}
}