(function () {

  tinymce.PluginManager.add('example', function (editor, url) {
    // Add a button that opens a window
    editor.addButton('example', {
      text : 'Загрузить картинку',
      icon : false,
      onclick : function () {
        // Open window
        editor.windowManager.open({
          title : 'Example plugin',
          body : [
            {
              type : 'textbox',
              name : 'attach_url',
              classes : "add-image-to-mce-url",
              subtype : 'hidden'
            },
            {
              type : 'textbox',
              name : 'attach_id',
              classes : "add-image-to-mce-id",
              subtype : 'hidden'
            },
            {
              type : 'container',
              name : 'container',
              label : 'Превью изображения',
              html : '<div style="position: relative; height: 100px; text-align: center;"><img src="" style="height: 100%;" class="add-image-to-mce"></div>'
            },
            {
              name : 'file',
              type : 'textbox',
              subtype : 'file',
              label : 'Загрузите картинку',
              onchange : function (event) {

                console.log(event);
                console.log(this);

                console.log(editor.windowManager.getParams('file'));

                var file = event.target.files[0];

                var img = $('img.add-image-to-mce')[0];
                var url = $('input.mce-add-image-to-mce-url')[0];
                var id = $('input.mce-add-image-to-mce-id')[0];

                var data = new FormData();
                data.append("Upload[attachments]", file);
                data.append('name', 'attachments');

                ajax('/admin/json/upload', data, function (response) {
                  //console.log(response);
                  if (response.success && !response.error) {
                    img.src = response.file.url;
                    img.setAttribute('data-id', response.file.id);

                    url.setAttribute('value', response.file.url);
                    id.setAttribute('value', response.file.id);
                  }
                }, function () {

                })
              }
            }
          ],
          onsubmit : function (e) {
            // Insert content when the window form is submitted
            editor.insertContent('<div><img class="img-responsive" src="' + e.data.attach_url + '"/></div>');
          }
        });
      }
    });

    // Adds a menu item to the tools menu
    editor.addMenuItem('example', {
      text : 'Example plugin',
      context : 'tools',
      onclick : function () {
        // Open window with a specific url
        editor.windowManager.open({
          title : 'TinyMCE site',
          url : 'http://www.tinymce.com',
          width : 800,
          height : 600,
          buttons : [{
            text : 'Close',
            onclick : 'close'
          }]
        });
      }
    });
  });

  // Load plugin specific language pack
  tinymce.PluginManager.requireLangPack('imagelight');

  tinymce.create('tinymce.plugins.ImagelightPlugin', {

    init : function (ed, url) {
      tinyMCE.activeEditor.execCommand('mceExample');
      ed.addCommand('mceImagelight', function () {
        ed.windowManager.open({
          file : '/Image',
          width : 480,
          height : 385,
          inline : 1
        }, {
          plugin_url : url // Plugin absolute URL
        });
      });

      ed.addButton('imagelight', {
        title : 'imagelight.desc',
        cmd : 'mceImagelight',
        image : url + '/img/imagelight.png'
      });

      ed.onNodeChange.add(function (ed, cm, n) {
        cm.setActive('imagelight', n.nodeName == 'IMG');
      });
    },

    getInfo : function () {
      return {
        longname : 'Imagelight plugin',
        author : 'AldeFalco',
        authorurl : 'http://tinymce.moxiecode.com',
        infourl : 'http://wiki.moxiecode.com/index.php/TinyMCE:Plugins/example',
        version : "1.0"
      };
    }
  });

  tinymce.PluginManager.add('imagelight', tinymce.plugins.ImagelightPlugin);
})();
