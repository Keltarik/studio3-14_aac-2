<?php

define('YII_login', true);

require(__DIR__ . '/../php/vendor/autoload.php');
require(__DIR__ . '/../php/vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../php/common/config/bootstrap.php');
//require(__DIR__ . '../php/frontend/config/bootstrap.php');

$config = yii\helpers\ArrayHelper::merge(
	require(__DIR__ . '/../php/common/config/main.php'),
	require(__DIR__ . '/../php/backend/config/main.php')
);

$application = new yii\web\Application($config);
$application->run();



