CREATE TABLE `tag` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` TEXT NULL,
	`slug` TEXT NULL,
	PRIMARY KEY (`id`),
	INDEX `id` (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `tag_link` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`tag_id` INT(11) UNSIGNED NOT NULL,
	`project_id` INT(10) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `id` (`id`),
	INDEX `FK_tag_link_tag` (`tag_id`),
	INDEX `FK_tag_link_project` (`project_id`),
	CONSTRAINT `FK_tag_link_project` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `FK_tag_link_tag` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
